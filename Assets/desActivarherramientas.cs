﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class desActivarherramientas : MonoBehaviour {

    private GameObject camaraherramientas;
	// Use this for initialization
	void Start () {
        camaraherramientas= GameObject.FindGameObjectWithTag("camaraHerramientas");
        if (camaraherramientas)
            camaraherramientas.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnDestroy()
    {
        if (camaraherramientas)
            camaraherramientas.SetActive(true);
    }
}
