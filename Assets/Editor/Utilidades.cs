﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Diagnostics;
using System.Timers;
using System.Threading;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class Utilidades
{
    //VARAIBLES

    //----------------------
    //---APP NAME
    const string COMPANY = "CloudLabs";

    //SPA
    static string PRODUCT_SPA = "CloudLabs Matemáticas - Simulador de trayectoria parabólica de un cometa - la parábola";
    static string PRODUCT_SPA_OSX = "CloudLabs - Conicas";
    static string PRODUCT_ENG = "CloudLabs - Parabolic trajectory of a comet";
    static string PRODUCT_ENG_OSX = "CloudLabs - Conic sections";
    static string PRODUCT_POR = "CloudLabs - Tamanho e forma de um planeta gasoso";
    static string PRODUCT_POR_OSX = "CloudLabs - Conicas";
    static string PRODUCT_TUR = "CloudLabs - Kuyruklu yildizin paraboli̇k yörüngesi̇";
    static string PRODUCT_TUR_OSX = "CloudLabs - Nombre en turco para osx";
    

    //----------------------
    //---identifier
    static string SPA_ID = "com.cloudlabs.linea.parabola.v4";
    static string ENG_ID = "com.cloudlabs.eng.linea.parabola.v4";
    static string POR_ID = "com.cloudlabs.por.linea.parabola.v4";
    static string TUR_ID = "com.cloudlabs.tur.linea.recta.v4";

    //----------------------
    //---LANG
    //static menu.LanguageList Lang_SPA = menu.LanguageList.Spanish;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_SPA = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.español;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_ENG = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.ingles;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_POR = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.portugues;
    static NSTraduccionIdiomas.ClsControladorIdiomas.idioma Lang_TUR = NSTraduccionIdiomas.ClsControladorIdiomas.idioma.turco;

    //---BIL LANG
    //static menu.ConfigureLanguageList SPA_ENG = menu.ConfigureLanguageList.Spanish_English;
    //static menu.ConfigureLanguageList POR_ENG = menu.ConfigureLanguageList.English_Portuguese;

    //----
    static bool SECURITY;

    //keyword
    static string simKeyword = "math_conic02";
    static string std_folder = @"DEPLOY\Release\STD\";
    static string mono_folder = @"DEPLOY\Release\MONO\";

    [MenuItem("IE/Seguridad")]
    public static void Seguridad()
    {

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = !GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;
        SECURITY = GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        if (GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch)
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "SEGURIDAD ACTIVADA", "ok");
        }
        else
        {
            EditorUtility.DisplayDialog("SEGURIDAD", "NOT", "ok");
        }

    }


    [MenuItem("IE/Build")]
    static void Build()
    {
        
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = true;
        SECURITY = GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;

        //sim_SPA_webGL();
        //sim_ENG_webGL();
        //sim_POR_webGL();
        //sim_TUR_webGL();

        //sim_SPA_webGL_aula();
        //sim_ENG_webGL_aula();
        // sim_POR_webGL_aula();

        //sim_SPA_webGL_aula_nuevo();
        //sim_ENG_webGL_aula_nuevo();
        //sim_TUR_webGL_aula_nuevo();
        sim_POR_webGL_aula_nuevo();

        //sim_SPA_PC();
        // //sim_SPA_PC_mono();
        //sim_ENG_PC();
        // //sim_ENG_PC_mono();
        sim_POR_PC();
        // sim_POR_PC_mono();
        //sim_TUR_PC();

        // //sim_SPA_OSX();
        // //sim_SPA_OSX_mono();
        //sim_ENG_OSX();
        // //sim_ENG_OSX_mono();
        sim_POR_OSX();
        // sim_POR_OSX_mono();
        //sim_TUR_OSX();

        //sim_SPA_APK();
        // //sim_SPA_APK_mono();
        //sim_ENG_APK();
        // //sim_ENG_APK_mono();
        //sim_POR_APK();
        // sim_POR_APK_mono();
        //sim_TUR_APK();

    }


    /*[MenuItem("IE/test")]
    static void test()
    {
        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);
    }*/


    static BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
    static void groupScenes()
    {


        buildPlayerOptionsScene.scenes = new[] { "Assets/Escenas/pruebas.unity"};
    }

   //Exportaciones
    //PC - SPA
    static void sim_SPA_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());



        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std/" + simKeyword + "_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_PC_mono()
    {
        
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        //BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, @"DEPLOY\MONO\autom_mesaplc_graf_spa_std\autom_mesaplc_graf_spa_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);
        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono/" + simKeyword + "_spa_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_std/" + simKeyword + "_eng_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_PC_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_mono/" + simKeyword + "_eng_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_std/" + simKeyword + "_por_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_PC_mono()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();

        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = POR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_mono/" + simKeyword + "_por_mono.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }


    //OSX - SPA
    static void sim_SPA_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_s_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_OSX_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_m_x/" + PRODUCT_SPA_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_s_x/" + PRODUCT_ENG_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_OSX_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_m_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_s_x/" + PRODUCT_POR_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_OSX_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = POR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_m_x/" + PRODUCT_ENG + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_OSX_mono.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }


    //APK - SPA
    static void sim_SPA_APK()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_SPA_APK_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, SPA_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_spa_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_APK()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, ENG_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_ENG_APK_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_ENG;
        PlayerSettings.applicationIdentifier = ENG_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, ENG_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_eng_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_APK()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, POR_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }
    static void sim_POR_APK_mono()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_ENG.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, POR_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = true;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, mono_folder + simKeyword + "_por_mono.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }


    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = true;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_lti/" + simKeyword + "_spa_lti/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    //[MenuItem("IE/sim_ENG_webGL")]
    static void sim_ENG_webGL()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = true;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_lti/" + simKeyword + "_eng_lti/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    //[MenuItem("IE/sim_ENG_webGL")]
    static void sim_POR_webGL()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = true;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }


    //[MenuItem("IE/sim_SPA_webGL")]
    static void sim_SPA_webGL_aula()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";


        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula/" + simKeyword + "_spa_webAula";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAula/" + simKeyword + "_spa_webAula/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }

    static void sim_POR_webGL_aula_nuevo()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_POR;
        PlayerSettings.applicationIdentifier = POR_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;
       
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_por_webAula_nuevo/" + simKeyword + "_por_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_webAula_nuevo/" + simKeyword + "_por_webAula_nuevo/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }

    static void sim_SPA_webGL_aula_nuevo()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;
       
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_SPA;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_spa_webAula_nuevo/" + simKeyword + "_spa_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_spa_webAula_nuevo/" + simKeyword + "_spa_webAula_nuevo/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }



    //[MenuItem("IE/sim_ENG_webGL")]
    static void sim_ENG_webGL_aula()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "http://servercloudlabs.com/validarSimulador/informacionUsuario";


        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAula/" + simKeyword + "_eng_webAula";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAula/" + simKeyword + "_eng_webAula/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());



    }

    static void sim_ENG_webGL_aula_nuevo()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_ENG;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_eng_webAula_nuevo/" + simKeyword + "_eng_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_eng_webAula_nuevo/" + simKeyword + "_eng_webAula_nuevo/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_ENG_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());



    }

    //[MenuItem("IE/sim_ENG_webGL")]
    static void sim_POR_webGL_aula()
    {
        /*PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_SPA;
        PlayerSettings.applicationIdentifier = SPA_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_POR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_por_lti/" + simKeyword + "_por_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_por_webAula/" + simKeyword + "_por_webAula/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_POR_std_webAULA.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }


    // Turco

    static void sim_TUR_webGL()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = true;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_TUR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_tur_lti/" + simKeyword + "_tur_lti";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_lti/" + simKeyword + "_tur_lti/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_TUR_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/

    }

    static void sim_TUR_webGL_aula_nuevo()
    {
        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;
        /**/
        //PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
        //PlayerSettings.WebGL.exceptionSupport = WebGLExceptionSupport.ExplicitlyThrownExceptionsOnly;
        //PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Gzip;

        //PlayerSettings.WebGL.memorySize = 512;
        //PlayerSettings.WebGL.template = "Simuladores";

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().LtiActivo = false;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().WebGLUrl = "https://menu.cloudlabs.us/validarSimulador/informacionUsuario";


        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_TUR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;

        //BuildPlayerOptions buildPlayerOptionsScene = new BuildPlayerOptions();
        string path = std_folder + simKeyword + "_tur_webAula_nuevo/" + simKeyword + "_tur_webAula_nuevo";

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_webAula_nuevo/" + simKeyword + "_tur_webAula_nuevo/" + "Html", BuildTarget.WebGL, BuildOptions.None);//@"Core\"

        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_TUR_std_webAULA_nuevo.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);/**/



        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().ModoAula = false;
        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());


    }


    static void sim_TUR_PC()
    {

        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_TUR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());



        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_std/" + simKeyword + "_tur_std.exe", BuildTarget.StandaloneWindows, BuildOptions.CompressWithLz4HC);


        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_TUR_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }


    static void sim_TUR_OSX()
    {
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(5).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_TUR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();


        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_s_x/" + PRODUCT_TUR_OSX + ".app", BuildTarget.StandaloneOSX, BuildOptions.CompressWithLz4HC);



        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_TUR_OSX_std.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

        //----------------------------------------------------------------------------------------
    }

    static void sim_TUR_APK()
    {
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        //-------------------------------------------------------------
        //CMD
        //Cambio de icono Xp_Electrical_Grafcet_SPA_PC
        ProcessStartInfo startInfo = new ProcessStartInfo(@"DEPLOY\Export\to_SPA.bat");
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo);

        //delay
        Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        AssetDatabase.Refresh();


        PlayerSettings.companyName = COMPANY;
        PlayerSettings.productName = PRODUCT_TUR;
        PlayerSettings.applicationIdentifier = TUR_ID;
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, TUR_ID);
        EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Internal;

        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().IdiomaActual = Lang_TUR;
        GameObject.FindObjectOfType<NSTraduccionIdiomas.ClsControladorIdiomas>().bilingueIngEsp = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().coneccionActiva = true;

        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().modoMonoUsuario = false;
        GameObject.FindObjectOfType<NsSeguridad.ClsSeguridad>().SeguridadSwitch = SECURITY;

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        groupScenes();

        BuildPipeline.BuildPlayer(buildPlayerOptionsScene.scenes, std_folder + simKeyword + "_tur_std.apk", BuildTarget.Android, BuildOptions.CompressWithLz4HC);


        //----------------------------------------------------------------------------------------
    }




    /*[MenuItem("IE/TEST")]
    static void sim_TEST()
    {
        //Empaquetado
        ProcessStartInfo startInfo2 = new ProcessStartInfo(@"DEPLOY\Export\Xp_SPA_std_weblti.bat");
        startInfo2.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(startInfo2);

    }*/
}