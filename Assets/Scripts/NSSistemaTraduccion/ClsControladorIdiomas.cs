﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfazAvanzada;
#if !UNITY_WEBGL
using ValidacionMenu;
#endif
using NSBoxMessage;
using NsSeguridad;


namespace NSTraduccionIdiomas
{
    public class ClsControladorIdiomas : MonoBehaviour
    {



        public DiccionarioIdiomas dic;
        public PanelInterfazSeleccionLenguajeInglesEspaniol panelseleccionIdioma;

        public ClsSeguridad seguridad;

        public bool bilingueIngEsp;

        //public GameObject panelAlerta;
        public BoxMessageManager boxMensajeSinLicencia;
        private bool darAcceso = false;
        private bool LTI_mode = false;

        public enum idioma
        {
            español,
            ingles,
            portugues,
            turco
        }

        public idioma IdiomaActual;

        private bool Acceso = false;


        private void Awake()
        {

        }


        void Start()
        {
            
#if !UNITY_WEBGL
            /*if (Validacion.Validar())
            {
                Acceso = true;
            }
            else
            {
                Acceso = false;
            }*/
#else
            //Acceso = true;
            LTI_mode = true;
#endif

            panelseleccionIdioma.gameObject.SetActive(false);
            activarVentanaCorrespondiente();  

        }

        void HandledelegateOnBoxMessageButtonAccept()
        {
            Application.Quit();
        }



        private void activarVentanaCorrespondiente()
        {
            
            StartCoroutine(activarPanel());

        }

        private IEnumerator activarPanel()
        {

            yield return new WaitForSeconds(0.5f);



            switch (IdiomaActual)
            {
                case idioma.español:
                    dic.SetIdioma(ListaIdiomas.espaniol);
                    break;
                case idioma.ingles:
                    dic.SetIdioma(ListaIdiomas.ingles);
                    break;
                case idioma.portugues:
                    dic.SetIdioma(ListaIdiomas.portugues);
                    break;
                case idioma.turco:
                    dic.SetIdioma(ListaIdiomas.turco);
                    break;
            }


            //if(LTI_mode==false){
            //
            //if (Acceso==true)
            //{

                //if (bilingueIngEsp)
                //{
                    //panelseleccionIdioma.ActivarPanel(true);
                //}
                //else
                //{
                    //PanelBienvenida.ActivarPanel(true);
                //}
            //}
            //else
            //{

                //boxMensajeSinLicencia.MtdCreateBoxMessageInfo(dic.Traducir("TextBoxAlertSeguridad", ""), dic.Traducir("TextAlertSeguridad", ""), HandledelegateOnBoxMessageButtonAccept);

            //}
            //
            //}else{
                if (bilingueIngEsp)
                {
                    panelseleccionIdioma.ActivarPanel(true);
                }
                else
                {
                seguridad.mtdIniciarSimulador();
                }
            //}

        }

    }
}
