﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSUtilidades
{
    public class RotarSkyBox : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            RenderSettings.skybox.SetFloat("_Rotation", Time.time*0.25f);
        }
    }

}