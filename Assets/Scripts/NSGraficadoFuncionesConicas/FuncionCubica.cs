﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionCubica : AbstractDibujoFuncion
	{


        #region members

        private float p;

        private float h;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        [SerializeField]
        private float velocidadObjetoOrbita = 0.25f;

        #endregion

        #region accesors
        public float _p
        {
            set
            {
                p = value;
            }
        }

        public float _h
        {
            set
            {
                h = value;
            }
        }

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpX = -1; tmpX <= 1; tmpX += 0.025f)
            {
                var tmpPointY = 4 * p * Mathf.Pow(tmpX, 3) - h;

                tmpPointY = Mathf.Clamp(tmpPointY, -1, 1);

                if (tmpPointY == -1 || tmpPointY == 1)
                    continue;

                tmpListaPosicionesGrafica.Add(new Vector2(tmpX, tmpPointY));
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {
            CrearLineRenderer("FuncionCubica", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

            var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
            tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);
        }
        #endregion

        #region courutines

        #endregion
    }
}