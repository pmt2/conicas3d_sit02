﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionElipse : AbstractDibujoFuncion
	{
        #region members

        private float h;

        private float k;

        private float a;

        private float b;

        [SerializeField]
        private GameObject prefabPlanetaFoco;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        [SerializeField]
        private GameObject prefabSol;

        private float velocidadObjetoOrbita = 0.25f;

        #endregion

        #region accesors

        public float _h
        {
            set
            {
                h = value;
            }
        }

        public float _k
        {
            set
            {
                k = value;
            }
        }

        public float _a
        {
            set
            {
                a = value;
            }
        }

        public float _b
        {
            set
            {
                b = value;
            }
        }

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            for (float tmpAnguloCircunferencia = 0; tmpAnguloCircunferencia <= 360; tmpAnguloCircunferencia += 1f)
            {
                var tmpFactorAncho = 0f;
                var tmpPoint = Vector2.zero;

                if (a > b)
                {
                    tmpFactorAncho = b / a;
                    tmpPoint = new Vector2(h + Mathf.Cos(tmpAnguloCircunferencia * Mathf.Deg2Rad) * a, k + tmpFactorAncho * Mathf.Sin(tmpAnguloCircunferencia * Mathf.Deg2Rad) * a);
                }
                else
                {
                    tmpFactorAncho = a / b;
                    tmpPoint = new Vector2(h + tmpFactorAncho * Mathf.Cos(tmpAnguloCircunferencia * Mathf.Deg2Rad) * b, k + Mathf.Sin(tmpAnguloCircunferencia * Mathf.Deg2Rad) * b);
                }

                tmpListaPosicionesGrafica.Add(tmpPoint);
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {
            

            CrearLineRenderer("Elipse", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

            var tmpPlanetaFoco = Instantiate(prefabPlanetaFoco, transform).GetComponent<Transform>();
            tmpPlanetaFoco.localPosition = new Vector2(h, k);

            var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
            tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);
        }
        #endregion

        #region courutines

        #endregion
    }
}