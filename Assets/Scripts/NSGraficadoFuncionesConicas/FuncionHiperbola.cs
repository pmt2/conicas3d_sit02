﻿using NSGraficadoFuncionesConicas.FuncionAleatoria;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
	public class FuncionHiperbola : AbstractDibujoFuncion
	{

        #region members

        private float h;

        private float k;

        private float a;

        private float b;

        private SentidoHiperbola sentidoHiperbola;

        private Vector2 focoHiperbola;

        [SerializeField]
        private GameObject prefabPlanetaFoco;

        [SerializeField]
        private GameObject prefabObjetoOrbita;

        private float velocidadObjetoOrbita = 0.25f;
        #endregion

        #region accesors

        public float _h
        {
            set
            {
                h = value;
            }
        }

        public float _k
        {
            set
            {
                k = value;
            }
        }

        public float _a
        {
            set
            {
                a = value;
            }
        }

        public float _b
        {
            set
            {
                b = value;
            }
        }

        public SentidoHiperbola _sentidoHiperbola
        {
            set
            {
                sentidoHiperbola = value;
            }
        }
        
        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override Vector3[] CalcularCordenadasFuncionMatematica()
        {
            var tmpListaPosicionesGrafica = new List<Vector3>();

            switch (sentidoHiperbola)
            {
                ///vertical
                case SentidoHiperbola.vertical:

                    for (float tmpValueX = -1f; tmpValueX <= 1f; tmpValueX += 0.025f)
                    {
                        var tmpValueY = ((a * Mathf.Sqrt(b * b + Mathf.Pow(tmpValueX - h, 2))) / b) + k;
                        tmpValueY = Mathf.Clamp(tmpValueY, -1, 1);

                        if (tmpValueY == 1 || tmpValueY == -1)
                            continue;

                        tmpListaPosicionesGrafica.Add(new Vector3(tmpValueX, tmpValueY));
                    }

                    focoHiperbola = new Vector3(h, k + Mathf.Sqrt(a * a + b * b));
                    break;

                ///horizontal
                case SentidoHiperbola.horizontal:

                    for (float tmpValueY = -1f; tmpValueY <= 1f; tmpValueY += 0.025f)
                    {
                        var tmpValueX = ((a * Mathf.Sqrt(b * b + Mathf.Pow(tmpValueY - k, 2))) / b) + h;
                        tmpValueX = Mathf.Clamp(tmpValueX, -1, 1);

                        if (tmpValueX == 1 || tmpValueX == -1)
                            continue;

                        tmpListaPosicionesGrafica.Add(new Vector3(tmpValueX, tmpValueY));
                    }

                    focoHiperbola = new Vector3(h + Mathf.Sqrt(a * a + b * b), k);
                    break;
            }

            posicionesRutaXY = tmpListaPosicionesGrafica.ToArray();
            return posicionesRutaXY;
        }

        public override void DibujarFuncion(Material argMaterialDibujo, bool argDibujarProgresivamente = true)
        {           
            CrearLineRenderer("Parabola", argMaterialDibujo, transform);
            SetPositionLineRenderer(posicionesRutaXY, argDibujarProgresivamente);

            var tmpPlanetaFoco = Instantiate(prefabPlanetaFoco, transform).GetComponent<Transform>();
            tmpPlanetaFoco.localPosition = focoHiperbola;

            var tmpCuerpoCeleste = Instantiate(prefabObjetoOrbita, transform).GetComponent<CuerpoCeleste>();
            tmpCuerpoCeleste.EjecutarMovimientoCuerpoCelesteRutaCiclica(posicionesRutaXY, velocidadObjetoOrbita);
        }
        #endregion

        #region courutines

        #endregion
    }
}