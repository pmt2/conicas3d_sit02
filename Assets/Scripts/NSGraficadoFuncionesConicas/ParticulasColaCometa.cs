﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSGraficadoFuncionesConicas
{
    public class ParticulasColaCometa : MonoBehaviour
    {
        #region members

        private Transform transformSol;

        #endregion

        private void Start()
        {
            transformSol = GameObject.FindGameObjectWithTag("SolSistemaSolar").transform;
        }

        // Update is called once per frame
        void Update()
        {
            MirarSol();
        }

        #region private methods

        private void MirarSol()
        {
            transform.rotation = Quaternion.LookRotation(transformSol.position - transform.position);
        }

        #endregion
    }
}
