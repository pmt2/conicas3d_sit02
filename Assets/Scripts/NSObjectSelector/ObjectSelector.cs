﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSObjectSelector
{
    public class ObjectSelector : MonoBehaviour
    {

        #region members
        /// <summary>
        /// camara activa que resivira la posicion del mause 
        /// </summary>
        public Camera camaraActiva;
        /// <summary>
        /// Token actualmente seleccionado
        /// </summary>
        private GameObject objectSelected;
        /// <summary>
        /// Layer en donde se encuentran los Tokens
        /// </summary>
        [SerializeField]
        private LayerMask layerObjects;
        /// <summary>
        /// Diferencia de distancia desde la seleccion del click
        /// </summary>
        private Vector3 differenceDistanceObject;
        /// <summary>
        /// nombre 
        /// </summary>
        public string tacObjeto;
        #endregion

        #region monoBehaviour

        // Update is called once per frame
        void Update()
        {
            SelectObject();
        }

        #endregion

        #region private methods

        private void SelectObject()
        {/*
            var tmpDraw = camaraActiva.ScreenToWorldPoint(Input.mousePosition);
            var aux = new Vector3(tmpDraw.x, 0, tmpDraw.z);

            Debug.DrawLine(new Vector3(aux.x + this.transform.position.x, aux.z + this.transform.position.y, aux.z + this.transform.position.z), aux + Vector3.right,Color.blue);
            */
            if (objectSelected == null)
            {

                if (Input.GetMouseButtonDown(0))
                {
                    var auxr = camaraActiva.ScreenToWorldPoint(Input.mousePosition);
                    var tmpPosicionOnWorld = new Vector3(auxr.x, 0, auxr.z);
                    var Ray = camaraActiva.ScreenPointToRay(Input.mousePosition);
                   // Debug.Log(camaraActiva.name);
                    //Debug.Log(Ray.origin);
                    RaycastHit hit;
                    Debug.DrawRay(new Vector3(Ray.origin.x+ this.transform.position.x, Ray.origin.y+ this.transform.position.y, Ray.origin.z+ this.transform.position.z), Ray.direction ,Color.green);
                    if (Physics.Raycast(Ray, out hit, 10f, layerObjects))
                    {
                        Debug.Log(hit.transform.gameObject.name);
                        if (hit.collider.gameObject.tag == tacObjeto)
                        {

                            objectSelected = hit.collider.gameObject;
                            var tmpInterfaceObjectSelected = objectSelected.GetComponent<IObjectSelectable>();

                            if (!tmpInterfaceObjectSelected.GetIsDraggable())
                                return;

                            tmpInterfaceObjectSelected.SetSelected(true);
                            differenceDistanceObject = objectSelected.transform.position - tmpPosicionOnWorld;//diferencia desde el punto de colision hasta 
                        }
                    }
                }
            }
            else
            {

                var tmpInterfaceObjectSelected = objectSelected.GetComponent<IObjectSelectable>();

                if (Input.GetMouseButtonUp(0))
                {
                    tmpInterfaceObjectSelected.SetSelected(false);
                    objectSelected = null;
                    return;
                }
                if (tmpInterfaceObjectSelected.GetIsDraggable())
                {
                    var tmpRayCast = camaraActiva.ScreenPointToRay(Input.mousePosition);
                    var tmpPlane = new Plane(-Vector3.up, Vector3.zero);
                    var tmpDistanceCut = 0f;
                    tmpPlane.Raycast(tmpRayCast, out tmpDistanceCut);
                    tmpInterfaceObjectSelected.SetDraggedPosition(tmpRayCast.origin + tmpRayCast.direction.normalized * tmpDistanceCut,this.transform.position);

                }
            }
        }

        #endregion
    }
}