﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSCreacionPDF;
using NSInterfazAvanzada;
using NSTraduccionIdiomas;

public class clsCuaderno : MonoBehaviour {

    #region members
    public Text Textpregunta;
    public InputField cuaderno;
    public Text TextoPaginaActual;
    public int NumeroDeCaracteres;
    public string[] preguntasComplementariasRecta;
    public string[] preguntasComplementariasParabola;
    public string[] preguntasComplementariasCircunferencia;
    public string[] preguntasComplementariasElipse;
    public string[] preguntasComplementariasHiperbola;
    public DiccionarioIdiomas Dic;
    public NSInterfazAvanzada.PanelInterfazBotonesSeleccionSituacion situasionseleccionada;
    public NSCreacionPDF.ControladorValoresPDF pdfManger;
    public Sprite[] botonEliminar;
    public Sprite[] botonHojaNUeva;

    private List<string> HojasNuevas;

    [SerializeField]
    private Button BTeleiminar;
    [SerializeField]
    private Button BTNuevaHoja;

    private List<string> preguntasDeMas;
    private List<string> ListRespuestas;
    private int HojaActual;
    private int numPreguntas;
    private SituacionSeleccionada situacionActual;

    private int OjasENMemoria=4;
    #endregion

    #region monoBehaviour

    private void Awake()
    {

    }

    // Use this for initialization
    void Start() {


    }

    // Update is called once per frame
    void Update() {

        TextoPaginaActual.text = Dic.Traducir( "textPagina","" )+ (HojaActual+1);

        if ((ListRespuestas.Count <= HojaActual+1) && HojaActual  == (ListRespuestas.Count-1)+HojasNuevas.Count)
        {
            if (HojasNuevas.Count< OjasENMemoria) {
                BTNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[1];
                BTNuevaHoja.enabled = true;
            }
        }
        else
        {
            BTNuevaHoja.gameObject.GetComponent<Image>().sprite = botonHojaNUeva[0];
            BTNuevaHoja.enabled = false;
        }


        if (ListRespuestas.Count <= HojaActual && HojasNuevas.Count >= 1)
        {
            BTeleiminar.GetComponent<Image>().sprite = botonEliminar[1];
            BTeleiminar.enabled = true;
        }

        else
        {
            BTeleiminar.GetComponent<Image>().sprite = botonEliminar[0];
            BTeleiminar.enabled = false;
        }
            


    }

    #endregion


    #region private methods

    /// <summary>
    /// funsion que inicialisa la lista de preguntas
    /// </summary>
    private void MtdInitRespuestas()
    {

        switch (situasionseleccionada._funcionSeleccionada) {

            case SituacionSeleccionada.recta:
                mtdllenarlista(preguntasComplementariasRecta);
                break;
            case SituacionSeleccionada.parabola:
                mtdllenarlista(preguntasComplementariasParabola);
                break;
            case SituacionSeleccionada.circunferencia:
                mtdllenarlista(preguntasComplementariasCircunferencia);
                break;
            case SituacionSeleccionada.elipse:
                mtdllenarlista(preguntasComplementariasElipse);
                break;
            case SituacionSeleccionada.hiperbola:
                mtdllenarlista(preguntasComplementariasHiperbola);
                break;
        }

    }

    /// <summary>
    /// Inicializa la lista que contendra las respuestas del usuario
    /// </summary>
    /// <param name="preguntas"></param>
    private void mtdllenarlista(string[] preguntas)
    {
        ListRespuestas.Clear();
        numPreguntas=0;
        for (int i = 0; i < preguntas.Length; i++)
        {
            numPreguntas++;
            ListRespuestas.Add("");
        }

    }

    /// <summary>
    /// pone el texto de respues correspondiente a cada pregunta 
    /// </summary>
    private void mtdActualisarRespuesta()
    {

        if (HojaActual < ListRespuestas.Count)
        {
            cuaderno.text = ListRespuestas[HojaActual];
        }
        else
        {
            cuaderno.text = HojasNuevas[HojaActual- ListRespuestas.Count];
        }
    }


    //---------------------------------------------------------------------------------------

    /// <summary>
    ///  pinta la hoja del cuaderno actual con la pregunta correspondiente
    /// </summary>
    private void mtdPintarPregunta()
    {
        
        switch (situasionseleccionada._funcionSeleccionada)
        {
            case SituacionSeleccionada.recta:
                if (HojaActual < preguntasComplementariasRecta.Length)
                {
                    Textpregunta.text = Dic.Traducir("PreguntasComplementariasRecta" + (HojaActual + 1), preguntasComplementariasRecta[HojaActual]);
  
                }
                else {
                    Textpregunta.text = "";
                }
                situacionActual =  SituacionSeleccionada.recta;
                break;
            case SituacionSeleccionada.parabola:
                if (HojaActual < preguntasComplementariasParabola.Length)
                {
                    Textpregunta.text = Dic.Traducir("PreguntasComplementariasParabola" + (HojaActual + 1), preguntasComplementariasParabola[HojaActual]);

                }
                else
                {
                    Textpregunta.text = "";
                }
                situacionActual = SituacionSeleccionada.parabola;
                break;
            case SituacionSeleccionada.circunferencia:
                
                if (HojaActual < preguntasComplementariasCircunferencia.Length)
                {
                    Textpregunta.text = Dic.Traducir("PreguntasComplementariasCircunferencia" + (HojaActual + 1), preguntasComplementariasCircunferencia[HojaActual]);

                }
                else
                {
                    Textpregunta.text = "";
                }
                situacionActual = SituacionSeleccionada.circunferencia;
                break;
            case SituacionSeleccionada.elipse:
                
                if (HojaActual < preguntasComplementariasElipse.Length)
                {
                    Textpregunta.text = Dic.Traducir("PreguntasComplementariasElipse" + (HojaActual + 1), preguntasComplementariasElipse[HojaActual]);

                }
                else
                {
                    Textpregunta.text = "";
                }
                situacionActual = SituacionSeleccionada.elipse;
                break;
            case SituacionSeleccionada.hiperbola:

                if (HojaActual < preguntasComplementariasHiperbola.Length)
                {
                    Textpregunta.text = Dic.Traducir("PreguntasComplementariasHiperbola" + (HojaActual + 1), preguntasComplementariasHiperbola[HojaActual]);

                }
                else
                {
                    Textpregunta.text = "";
                }
                situacionActual = SituacionSeleccionada.hiperbola;
                break;
        }

    }

    private void MtdReposicionarInput()
    {
        Debug.Log("tamaño de las preguntas"+  Textpregunta.text.Length);

        var tam = Textpregunta.text.Length;
        if (tam > NumeroDeCaracteres)
        {
            if (tam > NumeroDeCaracteres*2)
            {
                if (tam > NumeroDeCaracteres*3)
                {
                }
                else
                {
                    cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 478);
                }
            }
            else
            {
                cuaderno.GetComponent<RectTransform>().sizeDelta= new Vector2( 1291.3f, 524);
            }
        }
        else
        {

            cuaderno.GetComponent<RectTransform>().sizeDelta = new Vector2(1291.3f, 569);
        }
    }



    #endregion

    #region public methods
    /// <summary>
    /// inicializa el cuaderno 
    /// </summary>
    public void initCuadreno() {
        HojasNuevas = new List<string>();
        numPreguntas = 0;
        HojaActual = 0;
        ListRespuestas = new List<string>();

        Debug.Log("ya tengo cargada la matriz");
        cuaderno.text = "";
        MtdInitRespuestas();
        mtdPintarPregunta();
        Dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        MtdReposicionarInput();
    }


    /// <summary>
    /// guarda cada vez que hay un cambio en el texto de respuesta 
    /// </summary>
    public void mtdguardarCambios()
    {
        Debug.Log("entre a guardar");
        int tamActual = cuaderno.text.Length;
        int tamANterior=0;
        if (HojaActual < ListRespuestas.Count)
            tamANterior = ListRespuestas[HojaActual].Length;

        if (HojaActual < ListRespuestas.Count)
        {
            if (tamActual != tamANterior)
            {
                ListRespuestas[HojaActual] = cuaderno.text;
            }
        }
        else
        {
            HojasNuevas[HojaActual- ListRespuestas.Count] = cuaderno.text;
        }

       // Debug.Log(ListRespuestas[HojaActual]);

    }


    public void btnAdelante()
    {
        if (numPreguntas+HojasNuevas.Count - 1 == HojaActual)
        {
            Debug.Log("no hay mas hojas");
        }
        else {
            HojaActual++;
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }
        MtdReposicionarInput();
    }

    public void btnAtras()
    {
        if (0 == HojaActual)
        {
            Debug.Log("no hay mas hojas");
        }
        else
        {
            HojaActual=HojaActual-1;
            mtdPintarPregunta();
            mtdActualisarRespuesta();
        }
        MtdReposicionarInput();
    }


    public void reiniciarCuaderno() {
        cuaderno.text = "";
        MtdInitRespuestas();
        MtdReposicionarInput();
        HojasNuevas.Clear();

    }

    public void actualizarCuaderno() {

        if (situacionActual!=situasionseleccionada._funcionSeleccionada)
        {
            Debug.Log("estoy entrando a borrar los datos");
            numPreguntas = 0;
            HojaActual = 0;
            ListRespuestas = new List<string>();
            HojasNuevas = new List<string>();
            MtdInitRespuestas();
            mtdPintarPregunta();
            cuaderno.text = "";
        }
        MtdReposicionarInput();
    }

    /// <summary>
    /// metodo que pasa los valoresd de las preguntas al pdf 
    /// </summary>
    public void mtdPasarInfoAlPdf()
    {
        bool HayHojasCreadas=false;
        string[] PregRespuesta;
        if (HojasNuevas.Count > 0)
        {
            PregRespuesta = new string[ListRespuestas.Count+ HojasNuevas.Count];
            HayHojasCreadas = true;
        }
        else
        {
             PregRespuesta = new string[ListRespuestas.Count];
        }
      

        for (int i=0; i < ListRespuestas.Count; i++)
        {
            switch (situacionActual)
            {
                case SituacionSeleccionada.recta:
                    PregRespuesta[i] = Dic.Traducir("PreguntasComplementariasRecta" + (i + 1),preguntasComplementariasRecta[i] )+"\n"+ ListRespuestas[i];
                    break;
                case SituacionSeleccionada.parabola:
                    PregRespuesta[i] = Dic.Traducir("PreguntasComplementariasParabola" + (i + 1), preguntasComplementariasParabola[i]) + "\n" + ListRespuestas[i];
                    break;
                case SituacionSeleccionada.circunferencia:
                    PregRespuesta[i] = Dic.Traducir("PreguntasComplementariasCircunferencia" + (i + 1), preguntasComplementariasCircunferencia[i]) + "\n" + ListRespuestas[i];           
                    break;
                case SituacionSeleccionada.elipse:
                    PregRespuesta[i] = Dic.Traducir("PreguntasComplementariasElipse" + (i + 1), preguntasComplementariasElipse[i]) + "\n" + ListRespuestas[i]; 
                    break;
                case SituacionSeleccionada.hiperbola:
                    PregRespuesta[i] = Dic.Traducir("PreguntasComplementariasHiperbola" + (i + 1), preguntasComplementariasHiperbola[i]) + "\n" + ListRespuestas[i]; 
                    break;
            }
        }
        if (HayHojasCreadas)
        {
            for (int i = 0; i< HojasNuevas.Count; i++)
            {

                PregRespuesta[i + ListRespuestas.Count] = HojasNuevas[i];
            }

        }

        pdfManger.SetPreguntasCuaderno(PregRespuesta);
    }

    public void mtdCrearNuevaHojas() {
        string aux = "";
        HojasNuevas.Add(aux);
        HojaActual=HojasNuevas.Count+ (ListRespuestas.Count-1);
        mtdPintarPregunta();
        mtdActualisarRespuesta();
    }

    public void mtdEliminarHojaNueva()
    {

        HojasNuevas.RemoveAt(HojaActual- ListRespuestas.Count);
        HojaActual--;
        mtdPintarPregunta();
        mtdActualisarRespuesta();
    }

    #endregion



}
