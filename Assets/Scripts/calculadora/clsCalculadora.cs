﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clsCalculadora : MonoBehaviour {

    /// <summary>
    /// texto que muestra el resulta de la calculadora 
    /// </summary>
    public Text textoCalculadora;

    /// <summary>
    /// numero pequeño del valor
    /// </summary>
    public Text textoValorGuardado;

    /// <summary>
    /// Valor actual de la calculadora
    /// </summary>
    public double valor;
    
    /// <summary>
    /// Valor actual de la calculadora en texto
    /// </summary>
    public string valorTexto;
    
    /// <summary>
    /// cantidad maxima de digitos permidos en la calculadora
    /// </summary>
    const int maxDigitos = 12;
    
    /// <summary>
    /// para saber si el valor de la calculadora tiene punto
    /// </summary>
    bool tienePunto = false;
    
    /// <summary>
    /// valor de la calculadora guardado que se usa para operar el valor actual con el guardado
    /// </summary>
    double valorGuardado;
    
    /// <summary>
    /// operacion actual de la calculadora
    /// </summary>
    private Operacion operacion = Operacion.Ninguna;
    
    /// <summary>
    /// enum con las posibles operaciones de la calculadora
    /// </summary>
    enum Operacion { Ninguna, Sumar, Restar, Dividir, Mult }

    void Start()
    {
        Reiniciar();
    }


    void Update()
    {

    }

    /// <summary>
    /// Reinicia los valores de la calculadora
    /// </summary>
    private void Reiniciar()
    {
        valorGuardado = 0;
        valor = 0;
        valorTexto = "0";
        textoCalculadora.text = valorTexto;
        textoValorGuardado.text = "";
        tienePunto = false;
        operacion = Operacion.Ninguna;
    }

    /// <summary>
    /// Actualiza el valor en la pantalla de la calculadora
    /// </summary>
    private void ActualizarValor()
    {
      if (valorTexto.Length > maxDigitos)
        {
            valorTexto = valorTexto.Substring(0, maxDigitos);
            textoCalculadora.text = valorTexto;
        }

        bool puntoAlFinal = false;

        if (valorTexto.Length > 0 && tienePunto && valorTexto[valorTexto.Length - 1] == '.')
        {
            puntoAlFinal = true;
        }

        if (valorTexto == "0.")
            valorTexto = "0.";
        else if (valorTexto == "0.0")
            valorTexto = "0.0";
        else if (valorTexto == "0.00")
            valorTexto = "0.00";
        else if (valorTexto == "0.000")
            valorTexto = "0.000";
        else if (valorTexto == "0.0000")
            valorTexto = "0.0000";
        else if (valorTexto == "0.00000")
            valorTexto = "0.00000";
        else if (valorTexto == "0.000000")
            valorTexto = "0.000000";
        else if (valorTexto == "0.0000000")
            valorTexto = "0.0000000";
        else if (valorTexto == "0.00000000")
            valorTexto = "0.00000000";
        else if (valorTexto == "0.000000000")
            valorTexto = "0.000000000";
        else if (valorTexto == "0.0000000000")
            valorTexto = "0.0000000000";
        else if (valorTexto == "0.00000000000")
            valorTexto = "0.000000000000";
        else
        {
            valor = double.Parse(valorTexto);
            if (valor < 1)
                valorTexto = valor.ToString("0.################");
            else
            {
                string valorStr = valor.ToString();
                valorTexto = puntoAlFinal ? valorStr + "." : valorStr;
            }
        }
        textoCalculadora.text = valorTexto;
    }

    /// <summary>
    /// Precion del boton 0
    /// </summary>
    public void Btn0()
    {
        valorTexto += "0";
        ActualizarValor();
    }

    public void Btn1()
    {
        valorTexto += "1";
        ActualizarValor();
    }

    public void Btn2()
    {
        valorTexto += "2";
        ActualizarValor();
    }

    public void Btn3()
    {
        valorTexto += "3";
        ActualizarValor();
    }

    public void Btn4()
    {
        valorTexto += "4";
        ActualizarValor();
    }

    public void Btn5()
    {
        valorTexto += "5";
        ActualizarValor();
    }

    public void Btn6()
    {
        valorTexto += "6";
        ActualizarValor();
    }

    public void Btn7()
    {
        valorTexto += "7";
        ActualizarValor();
    }

    public void Btn8()
    {
        valorTexto += "8";
        ActualizarValor();
    }

    public void Btn9()
    {
        valorTexto += "9";
        ActualizarValor();
    }

    public void BtnMas()
    {
        ResolverOperacion();
        GuardarValor();
        operacion = Operacion.Sumar;
    }

    public void BtnMenos()
    {
        ResolverOperacion();
        GuardarValor();
        operacion = Operacion.Restar;
    }

    public void BtnDiv()
    {
        ResolverOperacion();
        GuardarValor();
        operacion = Operacion.Dividir;
    }

    public void BtnMult()
    {
        ResolverOperacion();
        GuardarValor();
        operacion = Operacion.Mult;
    }

    public void BtnSin()
    {
        if (valor % 180 == 0)
        {
            valor = 0;
        }

        valor = valor * Mathf.Deg2Rad;
        valor = System.Math.Sin(valor);
        valor = (float)valor;
        valorTexto = valor.ToString();
        ActualizarValor();

    }

    public void BtnCos()
    {
        Debug.Log((valor + 90) % 180);

        if ((valor + 90) % 180 == 0)
        {
            valor = 0;
        }
        else
        {
            valor = 90 - valor;
        }

        Debug.Log("Valor " + valor);
        valor = valor * Mathf.Deg2Rad;
        valor = System.Math.Sin(valor);
        valor = (float)valor;
        valorTexto = valor.ToString();

        ActualizarValor();
    }

    public void BtnSqrt()
    {
        valor = System.Math.Sqrt(valor);
        valorTexto = valor.ToString();
        ActualizarValor();
    }

    public void BtnC()
    {
        Reiniciar();
    }


    public void BtnPunto()
    {
        if (!tienePunto)
        {
            tienePunto = true;
            valorTexto += ".";
            ActualizarValor();
        }
    }

    public void BtnIgual()
    {
        ResolverOperacion();
        ActualizarValor();
        valorGuardado = 0;
        textoValorGuardado.text = "";
    }

    void GuardarValor()
    {
        valorGuardado = valor;
        textoValorGuardado.text = valorGuardado.ToString();
        valorTexto = "0";
        tienePunto = false;
        ActualizarValor();
    }

    /// <summary>
    /// Resolver operacion que esta en cola
    /// </summary>
    public void ResolverOperacion()
    {
        switch (operacion)
        {
            case Operacion.Ninguna:
                break;
            case Operacion.Dividir:
                if (valor == 0)
                    valorGuardado = 0;
                else
                    valorGuardado = valorGuardado / valor;
                valor = valorGuardado;
                valorTexto = valor.ToString();
                break;
            case Operacion.Mult:
                valorGuardado = valorGuardado * valor;
                valor = valorGuardado;
                valorTexto = valor.ToString();
                break;
            case Operacion.Restar:
                valorGuardado = valorGuardado - valor;
                valor = valorGuardado;
                valorTexto = valor.ToString();
                break;
            case Operacion.Sumar:
                valorGuardado = valorGuardado + valor;
                valor = valorGuardado;
                valorTexto = valor.ToString();
                break;
        }
        operacion = Operacion.Ninguna;
    }

    public void mtdUIMas() {

        textoCalculadora.text += "+"; 

    }

    /* public void mtdsimbolosMate(){

         string str = "non-symbolic characters";

         Console.WriteLine(Char.IsSymbol('+'));      // Output: "True"
         Console.WriteLine(Char.IsSymbol(str, 8));

     }*/




}
