﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSPrueba
{
	public class TriggerPuerta : MonoBehaviour
	{

        #region members
        public int NumeroDePuerta;

        private Animator animatorPuerta;

        [SerializeField]
        private MeshRenderer refMeshRenderer;

        [SerializeField]
        private clsAdministradorSonido sonidos;

        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
            animatorPuerta = GetComponent<Animator>();
        }

 		// Use this for initialization
		void Start ()
		{
            //sonidos = GameObject.FindGameObjectWithTag("Sounds").GetComponent<clsAdministradorSonido>();

        }

		// Update is called once per frame
 		void Update ()
		{

		}

        private void OnTriggerEnter(Collider other)
        {
            if (other.name.Equals("Player"))
            {
                if (NumeroDePuerta == 1)
                    sonidos.activarSonidoPuerta1Abrir();
                else
                    sonidos.activarSonidoPuerta2Abrir();

                animatorPuerta.SetBool("abierta", true);
                refMeshRenderer.material.SetColor("_EmissionColor", Color.green);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.name.Equals("Player"))
            {
                if(NumeroDePuerta == 1)
                    sonidos.activarSonidoPuerta1Cerrar();
                else
                    sonidos.activarSonidoPuerta2Cerrar();

                animatorPuerta.SetBool("abierta", false);
                refMeshRenderer.material.SetColor("_EmissionColor", Color.yellow);
            }
        }

        #endregion

        #region private methods

        #endregion

        #region public methods

        #endregion

        #region courutines

        #endregion
    }
}