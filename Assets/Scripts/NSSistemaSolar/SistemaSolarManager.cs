﻿using NSEvaluacion;
using NSGraficadoFuncionesConicas;
using NSGraficadoFuncionesConicas.FuncionAleatoria;
using NSInterfazAvanzada;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSSistemaSolar
{
    public class SistemaSolarManager : MonoBehaviour
    {
        #region members

        [SerializeField]
        private Transform sol;

        [SerializeField]
        private Transform[] planetas;

        [SerializeField]
        private Transform[] orbitas;

        private PlanoCartesiano refPlanoCartesiano;

        private AccionesSistemaSolar accionActual= AccionesSistemaSolar.desactivado;

        [SerializeField]
        private GameObject prefabMeteoro;

        [SerializeField]
        private GameObject prefabLuna;

        [SerializeField]
        private GameObject prefabCometa;

        /// <summary>
        /// Conos de luz proyectores
        /// </summary>
        [SerializeField]
        private GameObject conosLuzProyectores;

        [SerializeField, Header("Camaras control escena"), Space(20)]
        private ControlCamaraSistemaSolar refControlCamaraSistemaSolar;

        [SerializeField]
        private ControlCamaraPrincipal refControlCamaraPrincipal;

        [SerializeField, Header("Control personaje"), Space(20)]
        private GameObject joyStickControl;

        [SerializeField, Header("Canvas"), Space(20)]
        private PanelInterfazBotonesSeleccionSituacion refPanelInterfazBotonesSeleccionSituacion;

        [SerializeField]
        private PanelInterfazPlano refPanelInterfazPlano;

        private LineRenderer lineRenderFuncionSimulacion;

        [SerializeField]
        private GameObject spritePlanoCartesiano;

        [SerializeField]
        private GameObject spriteGridPlanoCartesiano;

        /// <summary>
        /// Objeto que contiene la grafica de la situacion creada
        /// </summary>
        private GameObject graficaSituacionCreada;
        #endregion

        #region accesors

        #endregion

        #region monoBehaviour

        void Awake()
        {
            AsignarCuerposCelestes();
        }

        // Update is called once per frame
        void Update()
        {
           EjecutarAcciones();
        }

        #endregion

        #region private methods


        private void AsignarCuerposCelestes()
        {
            refPlanoCartesiano = GetComponent<PlanoCartesiano>();
            
            for (int i = 0; i < orbitas.Length; i++)
            {
                planetas[i].SetParent(orbitas[i]);

                for (int j = 0; j < orbitas[i].childCount; j++)
                    if (orbitas[i].GetChild(j).name.Equals("DibujarElipse(Clone)"))
                        Destroy(orbitas[i].GetChild(j).gameObject);            

               var tmpBoundOrbita = orbitas[i].GetComponent<Renderer>().bounds;
               var tmpFuncionElipse = refPlanoCartesiano.CrearFuncionElipse(0, 0, tmpBoundOrbita.extents[0], tmpBoundOrbita.extents[2], orbitas[i]);
               var tmpCuerpoCelestePlaneta = planetas[i].GetComponent<CuerpoCeleste>();                
               tmpCuerpoCelestePlaneta.EjecutarMovimientoCuerpoCelesteRutaCiclica(tmpFuncionElipse.GetCoordenadasXYZ(), 0.02f, Random.Range(0, 1000));
               orbitas[i].localRotation = Quaternion.Euler(Random.Range(-10f, 10f), 0, Random.Range(-10f, 10f));
            }

            planetas[2].localScale = Vector3.one * 0.075f;

            refControlCamaraSistemaSolar.SetObjetoFoco(sol);
            //accionActual = AccionesSistemaSolar.activado;
        }

        private void OcultarPlanetas(params bool[] argOcultarPlaneta)
        {
            for (int i = 0; i < argOcultarPlaneta.Length; i++)
                planetas[i].GetComponent<MeshRenderer>().enabled = !argOcultarPlaneta[i];
        }

        private void OcultarOrbitas(params bool[] argOcultarOrbita)
        {
            for (int i = 0; i < argOcultarOrbita.Length; i++)
                orbitas[i].gameObject.SetActive(!argOcultarOrbita[i]);
        }

        private void DetenerMovimientoPlanetas(params bool[] argOcultarPlaneta)
        {
            for (int i = 0; i < argOcultarPlaneta.Length; i++)
                planetas[i].GetComponent<CuerpoCeleste>()._ejecutarMovimiento = !argOcultarPlaneta[i];
        }

        private void EjecutarAcciones()
        {
            switch (accionActual)
            {
                case AccionesSistemaSolar.desactivado:
                    ScalarRotarSistemaSolar(new Vector3(0, 0.5f, 6.771f), Vector3.one * 0f, new Vector3(0, 0, 0));
                    break;

                case AccionesSistemaSolar.activado:
                    ScalarRotarSistemaSolar(new Vector3(0, 0.95f, 6.771f),  Vector3.one*2.5f, new Vector3(-30, 0, 0));
                    break;

                case AccionesSistemaSolar.visualizarSistemaSolar:
                    
                    break;

                case AccionesSistemaSolar.visualizarSituacion:                    
                    SeleccionarPlaneta();
                    break;

                case AccionesSistemaSolar.visualizarPlanoCartesiano:                   
                    break;
            }
        }

        private void SeleccionarPlaneta()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var tmpRayCast = refControlCamaraPrincipal.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                var tmpPlanetaSeleccionable = Physics.Raycast(tmpRayCast, Mathf.Infinity, LayerMask.GetMask("SistemaSolar"));

                if (tmpPlanetaSeleccionable)
                {
                    accionActual = AccionesSistemaSolar.visualizarPlanoCartesiano;
                    refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoquePlanoCartesianoPerspectiva);
                    refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfoquePrimeraPersona);
                    ResetearPosicionEscalaRotacion();
                    spritePlanoCartesiano.SetActive(true);
                    spriteGridPlanoCartesiano.SetActive(true);
                    OcultarOrbitas(true, true, true, true, true);
                    conosLuzProyectores.SetActive(false);
                    planetas[2].SetParent(transform);
                }
            }
        }

        private void ScalarRotarSistemaSolar(Vector3 argPosicion, Vector3 argScala, Vector3 argRotacion)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, argPosicion, 2f * Time.deltaTime);
            transform.localScale = Vector3.Lerp(transform.localScale, argScala, 2f * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.localRotation,Quaternion.Euler(argRotacion), 2f * Time.deltaTime);
        }

        private void ResetearPosicionEscalaRotacion()
        {
            transform.localPosition = new Vector3(0, -0.149f, 6.771f);
            transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;

            if (lineRenderFuncionSimulacion != null)
            {
                graficaSituacionCreada.transform.localRotation = Quaternion.identity;
                lineRenderFuncionSimulacion.transform.localRotation = Quaternion.identity;
                lineRenderFuncionSimulacion.transform.localScale = Vector3.one;
            }            
        }

        #endregion

        #region public methods

        public void CrearSituacionRecta(FuncionAleatoriaRecta argFuncionAleatoriaRecta)
        {
            ResetearPosicionEscalaRotacion();
            OcultarPlanetas(true, true, true, true, true);
            DetenerMovimientoPlanetas(true, true, true, true, true);

            var tmpFuncionRecta = refPlanoCartesiano.CrearFuncionRectaPendiente(argFuncionAleatoriaRecta._m, argFuncionAleatoriaRecta._b, transform);
            lineRenderFuncionSimulacion = tmpFuncionRecta.CrearLineRenderer("LineaRectaSituacion", refPlanoCartesiano._materialGraficaSimulacion, tmpFuncionRecta.transform, false);
            tmpFuncionRecta.SetPositionLineRenderer(tmpFuncionRecta.GetCoordenadasXYZ(true), false);

            //Objeto que orbita la ruta
            var tmpObjetoOrbita = tmpFuncionRecta.CrearObjetoOrbita(prefabMeteoro, lineRenderFuncionSimulacion.transform);
            tmpObjetoOrbita.EjecutarMovimientoCuerpoCelesteRutaPrincipioFin(tmpFuncionRecta.GetCoordenadasXYZ(true), 0.05f);

            //guardo la grafica de la funcion recien creada
            graficaSituacionCreada = tmpFuncionRecta.gameObject;
            accionActual = AccionesSistemaSolar.visualizarSituacion;

            //acciones camaras
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueHolograma);
            refControlCamaraSistemaSolar.SetObjetoFoco(tmpObjetoOrbita.transform);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfocarObjetivo);
        }

        public void CrearSituacionParabola(FuncionAleatoriaParabola argFuncionAleatoriaParabola)
        {
            ResetearPosicionEscalaRotacion();
            OcultarPlanetas(true, true, false, true, true);
            DetenerMovimientoPlanetas(true, true, true, true, true);            

            var tmpFuncionParabola = refPlanoCartesiano.CrearFuncionParabola(argFuncionAleatoriaParabola._h, argFuncionAleatoriaParabola._k, argFuncionAleatoriaParabola._p, transform, argFuncionAleatoriaParabola._direccionParabola);
            lineRenderFuncionSimulacion = tmpFuncionParabola.CrearLineRenderer("ParabolaSituacion", refPlanoCartesiano._materialGraficaSimulacion, tmpFuncionParabola.transform, false);           
            tmpFuncionParabola.SetPositionLineRenderer(tmpFuncionParabola.GetCoordenadasXYZ(true), false);

            //Objeto que orbita la ruta y planeta foco de la situacion
            var tmpObjetoOrbita = tmpFuncionParabola.CrearObjetoOrbita(prefabCometa, lineRenderFuncionSimulacion.transform);
            tmpObjetoOrbita.EjecutarMovimientoCuerpoCelesteRutaPrincipioFin(tmpFuncionParabola.GetCoordenadasXYZ(true), 0.05f);
            planetas[2].SetParent(tmpFuncionParabola.transform);
            planetas[2].localPosition = new Vector3(tmpFuncionParabola._focoParabola[0], 0, tmpFuncionParabola._focoParabola[1]);
            
            //guardo la grafica de la funcion recien creada
            graficaSituacionCreada = tmpFuncionParabola.gameObject;
            graficaSituacionCreada.transform.localRotation = orbitas[0].localRotation;
            accionActual = AccionesSistemaSolar.visualizarSituacion;

            //acciones camaras
            refControlCamaraSistemaSolar.SetObjetoFoco(tmpObjetoOrbita.transform);
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueHolograma);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfocarObjetivo);
        }

        public void CrearSituacionCircunferencia(FuncionAleatoriaCircunferencia argFuncionAleatoriaCircunferencia)
        {
            ResetearPosicionEscalaRotacion();
            OcultarPlanetas(true, true, false, true, true);
            DetenerMovimientoPlanetas(true, true, true, true, true);

            planetas[2].localScale = Vector3.one * argFuncionAleatoriaCircunferencia._r;
            planetas[2].localPosition = new Vector3(argFuncionAleatoriaCircunferencia._h, 0, argFuncionAleatoriaCircunferencia._k);

            accionActual = AccionesSistemaSolar.visualizarSituacion;

            //acciones camaras
            refControlCamaraSistemaSolar.SetObjetoFoco(planetas[2].transform);
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueHolograma);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfocarObjetivo);
        }

        public void CrearSituacionElipse(FuncionAleatoriaElipse argFuncionAleatoriaElipse)
        {
            ResetearPosicionEscalaRotacion();
            OcultarPlanetas(true, true, false, true, true);
            DetenerMovimientoPlanetas(true, true, true, true, true);

            var tmpFuncionElipse = refPlanoCartesiano.CrearFuncionElipse(argFuncionAleatoriaElipse._h, argFuncionAleatoriaElipse._k, argFuncionAleatoriaElipse._a, argFuncionAleatoriaElipse._b, transform);
            lineRenderFuncionSimulacion = tmpFuncionElipse.CrearLineRenderer("ElipseSituacion", refPlanoCartesiano._materialGraficaSimulacion, tmpFuncionElipse.transform, false);
            tmpFuncionElipse.SetPositionLineRenderer(tmpFuncionElipse.GetCoordenadasXYZ(true), false);

            planetas[2].SetParent(tmpFuncionElipse.transform);

            if (argFuncionAleatoriaElipse._a >= argFuncionAleatoriaElipse._b)
            {
                var tmpC = Mathf.Sqrt(argFuncionAleatoriaElipse._a * argFuncionAleatoriaElipse._a - argFuncionAleatoriaElipse._b * argFuncionAleatoriaElipse._b);
                planetas[2].localPosition = new Vector3(argFuncionAleatoriaElipse._h + tmpC, 0, argFuncionAleatoriaElipse._k);
            }
            else
            {
                var tmpC = Mathf.Sqrt(argFuncionAleatoriaElipse._b * argFuncionAleatoriaElipse._b - argFuncionAleatoriaElipse._a * argFuncionAleatoriaElipse._a);
                planetas[2].localPosition = new Vector3(argFuncionAleatoriaElipse._h, 0, argFuncionAleatoriaElipse._k + tmpC);
            }

            planetas[2].localScale = Vector3.one * 0.05f;

            //Objeto que orbita la ruta         
            var tmpObjetoOrbita = tmpFuncionElipse.CrearObjetoOrbita(prefabLuna, lineRenderFuncionSimulacion.transform);
            tmpObjetoOrbita.EjecutarMovimientoCuerpoCelesteRutaPrincipioFin(tmpFuncionElipse.GetCoordenadasXYZ(true), 0.05f);

            //guardo la grafica de la funcion recien creada
            graficaSituacionCreada = tmpFuncionElipse.gameObject;
            graficaSituacionCreada.transform.localRotation = orbitas[3].localRotation;

            accionActual = AccionesSistemaSolar.visualizarSituacion;

            //acciones camaras
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueHolograma);
            refControlCamaraSistemaSolar.SetObjetoFoco(tmpObjetoOrbita.transform);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfocarObjetivo);
        }

        public void CrearSituacionHiperbola(FuncionAleatoriaHiperbola argFuncionAleatoriaHiperbola)
        {
            ResetearPosicionEscalaRotacion();
            OcultarPlanetas(true, true, false, true, true);
            DetenerMovimientoPlanetas(true, true, true, true, true);

            var tmpFuncionHiperbola = refPlanoCartesiano.CrearFuncionHiperbola(argFuncionAleatoriaHiperbola._h, argFuncionAleatoriaHiperbola._k, argFuncionAleatoriaHiperbola._a, argFuncionAleatoriaHiperbola._b, transform, argFuncionAleatoriaHiperbola._sentidoHiperbola);
            lineRenderFuncionSimulacion = tmpFuncionHiperbola.CrearLineRenderer("HiperbolaSituacion", refPlanoCartesiano._materialGraficaSimulacion, tmpFuncionHiperbola.transform, false);            
            tmpFuncionHiperbola.SetPositionLineRenderer(tmpFuncionHiperbola.GetCoordenadasXYZ(true), false);

            planetas[2].SetParent(tmpFuncionHiperbola.transform);

            if (argFuncionAleatoriaHiperbola._sentidoHiperbola == SentidoHiperbola.horizontal)
                planetas[2].localPosition = new Vector3(argFuncionAleatoriaHiperbola._h + Mathf.Sqrt(argFuncionAleatoriaHiperbola._a* argFuncionAleatoriaHiperbola._a+ argFuncionAleatoriaHiperbola._b* argFuncionAleatoriaHiperbola._b), 0, argFuncionAleatoriaHiperbola._k);
            else
                planetas[2].localPosition = new Vector3(argFuncionAleatoriaHiperbola._h, 0, argFuncionAleatoriaHiperbola._k + Mathf.Sqrt(argFuncionAleatoriaHiperbola._a * argFuncionAleatoriaHiperbola._a + argFuncionAleatoriaHiperbola._b * argFuncionAleatoriaHiperbola._b));

            planetas[2].localScale = Vector3.one * Random.Range(0.05f, 0.09f);

            //Objeto que orbita la ruta         
            var tmpObjetoOrbita = tmpFuncionHiperbola.CrearObjetoOrbita(prefabCometa, lineRenderFuncionSimulacion.transform);
            tmpObjetoOrbita.EjecutarMovimientoCuerpoCelesteRutaPrincipioFin(tmpFuncionHiperbola.GetCoordenadasXYZ(true), 0.05f);

            //guardo la grafica de la funcion recien creada
            graficaSituacionCreada = tmpFuncionHiperbola.gameObject;
            graficaSituacionCreada.transform.localRotation = orbitas[4].localRotation;

            accionActual = AccionesSistemaSolar.visualizarSituacion;

            //acciones camaras
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueHolograma);
            refControlCamaraSistemaSolar.SetObjetoFoco(tmpObjetoOrbita.transform);
            refControlCamaraSistemaSolar.SetAccionActual(AccionesCamaraSistemaSolar.enfocarObjetivo);
        }

        public void ColisionMesaHolograma()
        {

            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoqueMesaHolograma);
            refPanelInterfazBotonesSeleccionSituacion.ActivarPanel(); 
            
            joyStickControl.SetActive(false);
            accionActual = AccionesSistemaSolar.visualizarSistemaSolar;
            Debug.Log("entre a el collider");
            refPanelInterfazBotonesSeleccionSituacion.OnButtonSeleccionSituacionParabola();

        }

        public void ActivarPanelInterfazPlano(bool argActivar)
        {
            refPanelInterfazPlano.ActivarPanel(argActivar);         
        }

        public void SalirSituacion()
        {
            accionActual = AccionesSistemaSolar.activado;
            OcultarPlanetas(false, false, false, false, false);
            DetenerMovimientoPlanetas(false, false, false, false, false);
            OcultarOrbitas(false, false, false, false, false);
            spritePlanoCartesiano.SetActive(false);
            spriteGridPlanoCartesiano.SetActive(false);
            AsignarCuerposCelestes();
            Destroy(graficaSituacionCreada);
            conosLuzProyectores.SetActive(true);

        }


        public void setCambiarAccion(AccionesSistemaSolar NuevaAccion)
        {
            accionActual = NuevaAccion;
        }


        #endregion
    }

    public enum AccionesSistemaSolar
    {
        desactivado,
        activado,
        visualizarSistemaSolar,
        visualizarSituacion,
        visualizarPlanoCartesiano
    }



}