﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSistemaSolar
{
	public class ControlCamaraPrincipal : MonoBehaviour
	{

        #region members


        [SerializeField]
        private Transform puntoEnfoqueHolograma;

        [SerializeField]
        private Transform puntoEnfoqueMesaHolograma;

        [SerializeField]
        private Transform puntoEnfoqueMesaSegundaSala;

        [SerializeField]
        private Transform puntoEnfoquePlanoCartesianoPerspectiva;

        [SerializeField]
        private AccionesCamaraPrincipal accionActual;

        private Camera refCamera;

        [SerializeField]
        private Camera refCameraSistemaSolar;

        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake ()
 		{
            refCamera = GetComponent<Camera>();
        }

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{
            EjecutarAcciones();
        }

        #endregion

        #region private methods

        private void EjecutarAcciones()
        {
            switch (accionActual)
            {                
                case AccionesCamaraPrincipal.enfoquePrimeraPersona:
                    EnfoquePrimerPersona();
                    break;

                case AccionesCamaraPrincipal.enfoqueMesaHolograma:
                    EnfoqueMesaHolograma();
                    break;

                case AccionesCamaraPrincipal.enfoqueMesaSegundaSala:
                    EnfoqueMesaSegundaSala();
                    break;

                case AccionesCamaraPrincipal.enfoqueHolograma:
                    EnfoqueHolograma();
                    break;

                case AccionesCamaraPrincipal.enfoquePlanoCartesianoPerspectiva:
                    EnfoquePlanoCartesianoPerspectiva();
                    break;

                case AccionesCamaraPrincipal.enfoquePlanoCartesianoOrtografico:
                    EnfoquePlanoCartesianoOrtografico();
                    break;
            }
        }

        private void EnfoquePrimerPersona()
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, 0.8f, 0), 0.2f);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.identity, 0.2f);
        }

        private void EnfoqueMesaHolograma()
        {
            transform.position = Vector3.Lerp(transform.position, puntoEnfoqueMesaHolograma.position, 0.2f);
            transform.rotation = Quaternion.Lerp(transform.rotation, puntoEnfoqueMesaHolograma.rotation, 0.2f);
        }

        private void EnfoqueMesaSegundaSala()
        {
            transform.position = Vector3.Lerp(transform.position, puntoEnfoqueMesaSegundaSala.position, 0.2f);
            transform.rotation = Quaternion.Lerp(transform.rotation, puntoEnfoqueMesaSegundaSala.rotation, 0.2f);
        }


        private void EnfoqueHolograma()
        {
            transform.position = Vector3.Lerp(transform.position, puntoEnfoqueHolograma.position, 0.05f);
            transform.rotation = Quaternion.Lerp(transform.rotation, puntoEnfoqueHolograma.rotation, 0.05f);
        }

        private void EnfoquePlanoCartesianoPerspectiva()
        {
            transform.position = Vector3.SlerpUnclamped(transform.position, puntoEnfoquePlanoCartesianoPerspectiva.position, 0.075f);
            transform.rotation = Quaternion.SlerpUnclamped(transform.rotation, puntoEnfoquePlanoCartesianoPerspectiva.rotation, 0.075f);

            if (Vector3.Distance(transform.position, puntoEnfoquePlanoCartesianoPerspectiva.position) <= 0.005f)
            {
                transform.position = puntoEnfoquePlanoCartesianoPerspectiva.position;
                transform.rotation = puntoEnfoquePlanoCartesianoPerspectiva.rotation;
                accionActual = AccionesCamaraPrincipal.enfoquePlanoCartesianoOrtografico;
                refCamera.fieldOfView = 60f;
                //refCamera.orthographic = true;
                refCameraSistemaSolar.orthographicSize = 0.75f;
                refCameraSistemaSolar.orthographic = true;
            }
        }

        private void EnfoquePlanoCartesianoOrtografico()
        {
            refCamera.fieldOfView = Mathf.Lerp(refCamera.orthographicSize, 65, 0.02f);
            refCamera.fieldOfView = Mathf.Clamp(refCamera.orthographicSize, 0, 64);
            refCameraSistemaSolar.orthographicSize =  Mathf.Lerp(refCameraSistemaSolar.orthographicSize, 1.1f, 0.05f);
            refCameraSistemaSolar.orthographicSize = Mathf.Clamp(refCameraSistemaSolar.orthographicSize, 0, 1.03f);

            if (refCameraSistemaSolar.orthographicSize == 1.03f)
            {
                accionActual = AccionesCamaraPrincipal.ningunaAccion;
                refSistemaSolarManager.ActivarPanelInterfazPlano(true);
            }
        }

        #endregion

        #region public methods

        public void SetAccionActual(AccionesCamaraPrincipal argAccionesCamaraPrincipal)
        {
            accionActual = argAccionesCamaraPrincipal;
        }

        public void SetOrthograpicCamera(bool argIsOrthograpic)
        {
            GetComponent<Camera>().orthographic = argIsOrthograpic;

            if (!argIsOrthograpic)
                GetComponent<Camera>().fieldOfView = 60;
        }
        #endregion

        #region courutines

        #endregion
    }

    public enum AccionesCamaraPrincipal
    {
        enfoqueMesaSegundaSala,
        ningunaAccion,
        enfoquePrimeraPersona,
        enfoqueMesaHolograma,
        enfoqueHolograma,
        enfoquePlanoCartesianoPerspectiva,
        enfoquePlanoCartesianoOrtografico
    }
}