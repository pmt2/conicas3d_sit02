﻿using NSBoxMessage;
using NSCreacionPDF;
using NSEvaluacion;
using NSGraficadoFuncionesConicas.FuncionAleatoria;
using NSSistemaSolar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSTraduccionIdiomas;

namespace NSInterfazAvanzada
{
	public class PanelInterfazBotonesSeleccionSituacion : AbstractPanelInterfaz
	{

        #region members
        private clsAdministradorSonido sonidos;

        [SerializeField, Header("Array funciones predefinidas"), Space(20)]
        private FuncionAleatoriaRecta[] arrayRectasPredefinidas;

        [SerializeField]
        private FuncionAleatoriaParabola[] arrayParabolasPredefinidas;

        [SerializeField]
        private FuncionAleatoriaCircunferencia[] arrayCircunferenciasPredefinidas;

        [SerializeField]
        private FuncionAleatoriaElipse[] arrayElipsesPredefinidas;

        [SerializeField]
        private FuncionAleatoriaHiperbola[] arrayHiperbolasPredefinidas;


        private List<FuncionAleatoriaRecta> listaRectasPredefinidasDisponibles = new List<FuncionAleatoriaRecta>();

        private List<FuncionAleatoriaParabola> listaParabolasPredefinidasDisponibles = new List<FuncionAleatoriaParabola>();

        private List<FuncionAleatoriaCircunferencia> listaCircunferenciasPredefinidasDisponibles = new List<FuncionAleatoriaCircunferencia>();

        private List<FuncionAleatoriaElipse> listaElipsesPredefinidasDisponibles = new List<FuncionAleatoriaElipse>();

        private List<FuncionAleatoriaHiperbola> listaHiperbolasPredefinidasDisponibles = new List<FuncionAleatoriaHiperbola>();


        private FuncionAleatoriaRecta funcionAleatoriaRectaSeleccionada;

        private FuncionAleatoriaParabola funcionAleatoriaParabolaSeleccionada;

        private FuncionAleatoriaCircunferencia funcionAleatoriaCircunferenciaSeleccionada;

        private FuncionAleatoriaElipse funcionAleatoriaElipseSeleccionada;

        private FuncionAleatoriaHiperbola funcionAleatoriaHiperbolaSeleccionada;

        [SerializeField]
        private SistemaSolarManager refSistemaSolarManager;

        private SituacionSeleccionada funcionSeleccionada;

        [SerializeField]
        private ControladorPreguntasSituacionales refControladorPreguntasSituacionales;

        [SerializeField]
        private ControlCamaraPrincipal refControlCamaraPrincipal;

        [SerializeField]
        private GameObject joyStick;

        [SerializeField]
        private PanelInterfazBotonesInterfazAfuera refPanelInterfazBotonesInterfazAfuera;

        [SerializeField]
        private BoxMessageManager refBoxMessageManager;

        private GameObject mensajeIngresarSituacion;

        [SerializeField]
        private PanelInterfazRegistroDatos refPanelInterfazRegistroDatos;

        [SerializeField]
        private Evaluacion refEvaluacion;

        [SerializeField]
        private CanvasReportePDF refCanvasReportePDF;

        private DiccionarioIdiomas DiccIdiomas;
        #endregion

        #region accesors

        public SituacionSeleccionada _funcionSeleccionada
        {
            get
            {
                return funcionSeleccionada;
            }
        }

        public FuncionAleatoriaRecta _funcionAleatoriaRectaSeleccionada
        {
            get
            {
                return funcionAleatoriaRectaSeleccionada;
            }
        }

        public FuncionAleatoriaParabola _funcionAleatoriaParabolaSeleccionada
        {
            get
            {
                return funcionAleatoriaParabolaSeleccionada;
            }
        }

        public FuncionAleatoriaCircunferencia _funcionAleatoriaCircunferenciaSeleccionada
        {
            get
            {
                return funcionAleatoriaCircunferenciaSeleccionada;
            }
        }

        public FuncionAleatoriaElipse _funcionAleatoriaElipseSeleccionada
        {
            get
            {
                return funcionAleatoriaElipseSeleccionada;
            }
        }

        public FuncionAleatoriaHiperbola _funcionAleatoriaHiperbolaSeleccionada
        {
            get
            {
                return funcionAleatoriaHiperbolaSeleccionada;
            }
        }

        #endregion

        #region MonoBehaviour
        private void Awake()
        {
            DiccIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }
        private void Start()
        {
            sonidos = GameObject.FindGameObjectWithTag("Sounds").GetComponent<clsAdministradorSonido>();
        }
        #endregion

        #region events


        #endregion

        #region private methods

        private FuncionAleatoriaRecta GetFuncionRectaAleatoria()
        {
            if (listaRectasPredefinidasDisponibles.Count == 0)
                listaRectasPredefinidasDisponibles.AddRange(arrayRectasPredefinidas);

            var tmpIndexFuncionAleatoria = Random.Range(0, listaRectasPredefinidasDisponibles.Count);
            var tmpFuncionAleatoria = listaRectasPredefinidasDisponibles[tmpIndexFuncionAleatoria];
            listaRectasPredefinidasDisponibles.RemoveAt(tmpIndexFuncionAleatoria);
            return tmpFuncionAleatoria;
        }

        private FuncionAleatoriaParabola GetFuncionParabolaAleatoria()
        {
            if (listaParabolasPredefinidasDisponibles.Count == 0)
                listaParabolasPredefinidasDisponibles.AddRange(arrayParabolasPredefinidas);

            var tmpIndexFuncionAleatoria = Random.Range(0, listaParabolasPredefinidasDisponibles.Count);
            var tmpFuncionAleatoria = listaParabolasPredefinidasDisponibles[tmpIndexFuncionAleatoria];
            listaParabolasPredefinidasDisponibles.RemoveAt(tmpIndexFuncionAleatoria);
            return tmpFuncionAleatoria;
        }

        private FuncionAleatoriaCircunferencia GetFuncionCircunferenciaAleatoria()
        {
            if (listaCircunferenciasPredefinidasDisponibles.Count == 0)
                listaCircunferenciasPredefinidasDisponibles.AddRange(arrayCircunferenciasPredefinidas);

            var tmpIndexFuncionAleatoria = Random.Range(0, listaCircunferenciasPredefinidasDisponibles.Count);
            var tmpFuncionAleatoria = listaCircunferenciasPredefinidasDisponibles[tmpIndexFuncionAleatoria];
            listaCircunferenciasPredefinidasDisponibles.RemoveAt(tmpIndexFuncionAleatoria);
            return tmpFuncionAleatoria;
        }

        private FuncionAleatoriaElipse GetFuncionElipseAleatoria()
        {
            if (listaElipsesPredefinidasDisponibles.Count == 0)
                listaElipsesPredefinidasDisponibles.AddRange(arrayElipsesPredefinidas);

            var tmpIndexFuncionAleatoria = Random.Range(0, listaElipsesPredefinidasDisponibles.Count);
            var tmpFuncionAleatoria = listaElipsesPredefinidasDisponibles[tmpIndexFuncionAleatoria];
            listaElipsesPredefinidasDisponibles.RemoveAt(tmpIndexFuncionAleatoria);
            return tmpFuncionAleatoria;
        }

        private FuncionAleatoriaHiperbola GetFuncionHiperbolaAleatoria()
        {
            if (listaHiperbolasPredefinidasDisponibles.Count == 0)
                listaHiperbolasPredefinidasDisponibles.AddRange(arrayHiperbolasPredefinidas);

            var tmpIndexFuncionAleatoria = Random.Range(0, listaHiperbolasPredefinidasDisponibles.Count);
            var tmpFuncionAleatoria = listaHiperbolasPredefinidasDisponibles[tmpIndexFuncionAleatoria];
            listaHiperbolasPredefinidasDisponibles.RemoveAt(tmpIndexFuncionAleatoria);
            return tmpFuncionAleatoria;
        }

        private void EntrarSituacionRecta()
        {
            funcionAleatoriaRectaSeleccionada = GetFuncionRectaAleatoria();
            refSistemaSolarManager.CrearSituacionRecta(funcionAleatoriaRectaSeleccionada);
            ActivarPanel(false);
            funcionSeleccionada = SituacionSeleccionada.recta;
            refControladorPreguntasSituacionales.SetPreguntasSituacion(funcionSeleccionada);
            refPanelInterfazRegistroDatos.PrepararInputsFieldNuevaSituacion();
            refEvaluacion.NuevaCalificacion();
            refCanvasReportePDF._codigoSituacion = "CALS3D102";
            refCanvasReportePDF._nombreSituacion = DiccIdiomas.Traducir("NombrePracticaRecta", "LineaRecta");
        }

        private void EntrarSituacionParabola()
        {
            funcionAleatoriaParabolaSeleccionada = GetFuncionParabolaAleatoria();
            refSistemaSolarManager.CrearSituacionParabola(funcionAleatoriaParabolaSeleccionada);
            ActivarPanel(false);
            funcionSeleccionada = SituacionSeleccionada.parabola;
            refControladorPreguntasSituacionales.SetPreguntasSituacion(funcionSeleccionada);
            refPanelInterfazRegistroDatos.PrepararInputsFieldNuevaSituacion();
            refEvaluacion.NuevaCalificacion();
            refCanvasReportePDF._codigoSituacion = "CALS3D102";
            refCanvasReportePDF._nombreSituacion = DiccIdiomas.Traducir("NombrePracticaParabola", "Parabola");
        }

        private void EntrarSituacionCircunferencia()
        {
            funcionAleatoriaCircunferenciaSeleccionada = GetFuncionCircunferenciaAleatoria();
            refSistemaSolarManager.CrearSituacionCircunferencia(funcionAleatoriaCircunferenciaSeleccionada);
            ActivarPanel(false);
            funcionSeleccionada = SituacionSeleccionada.circunferencia;
            refControladorPreguntasSituacionales.SetPreguntasSituacion(funcionSeleccionada);
            refPanelInterfazRegistroDatos.PrepararInputsFieldNuevaSituacion();
            refEvaluacion.NuevaCalificacion();
            refCanvasReportePDF._codigoSituacion = "CALS3D102";
            refCanvasReportePDF._nombreSituacion = DiccIdiomas.Traducir("NombrePracticaCircunferencia", "Circunferencia");
        }

        private void EntrarSituacionElipse()
        {
            funcionAleatoriaElipseSeleccionada = GetFuncionElipseAleatoria();
            refSistemaSolarManager.CrearSituacionElipse(funcionAleatoriaElipseSeleccionada);
            ActivarPanel(false);
            funcionSeleccionada = SituacionSeleccionada.elipse;
            refControladorPreguntasSituacionales.SetPreguntasSituacion(funcionSeleccionada);
            refPanelInterfazRegistroDatos.PrepararInputsFieldNuevaSituacion();
            refEvaluacion.NuevaCalificacion();
            refCanvasReportePDF._codigoSituacion = "CALS3D102";
            refCanvasReportePDF._nombreSituacion = DiccIdiomas.Traducir("NombrePracticaElipse", "Elipse");
        }

        private void EntrarSituacionHiperbola()
        {
            funcionAleatoriaHiperbolaSeleccionada = GetFuncionHiperbolaAleatoria();
            refSistemaSolarManager.CrearSituacionHiperbola(funcionAleatoriaHiperbolaSeleccionada);
            ActivarPanel(false);
            funcionSeleccionada = SituacionSeleccionada.hiperbola;
            refControladorPreguntasSituacionales.SetPreguntasSituacion(funcionSeleccionada);
            refPanelInterfazRegistroDatos.PrepararInputsFieldNuevaSituacion();
            refEvaluacion.NuevaCalificacion();
            refCanvasReportePDF._codigoSituacion = "CALS3D102";
            refCanvasReportePDF._nombreSituacion = DiccIdiomas.Traducir("NombrePracticaHiperbola", "Hiperbola");
        }

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        public void OnButtonSeleccionSituacionRecta()
        {
            EntrarSituacionRecta();
           // mensajeIngresarSituacion = refBoxMessageManager.MtdCreateBoxMessageInfo(DiccIdiomas.Traducir("TextAvisoPracticaRecta", "Práctica de laboratorio \n <b> Línea recta </b>"),DiccIdiomas.Traducir("TextBotonIngresar", "INGRESAR"), EntrarSituacionRecta, true);            
        }

        public void OnButtonSeleccionSituacionParabola()
        {
            EntrarSituacionParabola();
           // mensajeIngresarSituacion = refBoxMessageManager.MtdCreateBoxMessageInfo(DiccIdiomas.Traducir("TextAvisoPracticaParabola", "Práctica de laboratorio \n <b> Parábola </b>"), DiccIdiomas.Traducir("TextBotonIngresar", "INGRESAR"), EntrarSituacionParabola, true);
        }

        public void OnButtonSeleccionSituacionCircunferencia()
        {
            EntrarSituacionCircunferencia();
            //mensajeIngresarSituacion = refBoxMessageManager.MtdCreateBoxMessageInfo(DiccIdiomas.Traducir("TextAvisoPracticaCircunferencia", "Práctica de laboratorio \n <b> Circunferencia </b>"), DiccIdiomas.Traducir("TextBotonIngresar", "INGRESAR"), EntrarSituacionCircunferencia, true);
        }

        public void OnButtonSeleccionSituacionElipse()
        {
            EntrarSituacionElipse();
           // mensajeIngresarSituacion = refBoxMessageManager.MtdCreateBoxMessageInfo(DiccIdiomas.Traducir("TextAvisoPracticaElipse", "Práctica de laboratorio \n <b> Elipse </b>"), DiccIdiomas.Traducir("TextBotonIngresar", "INGRESAR"), EntrarSituacionElipse, true);
        }

        public void OnButtonSeleccionSituacionHiperbola()
        {
            EntrarSituacionHiperbola();
           // mensajeIngresarSituacion = refBoxMessageManager.MtdCreateBoxMessageInfo(DiccIdiomas.Traducir("TextAvisoPracticaHiperbola", "Práctica de laboratorio \n <b> Hipérbola </b>"), DiccIdiomas.Traducir("TextBotonIngresar", "INGRESAR"), EntrarSituacionHiperbola, true);
        }

        public void OnButtonSalirHolograma()
        {
            ActivarPanel(false);
            refControlCamaraPrincipal.SetAccionActual(AccionesCamaraPrincipal.enfoquePrimeraPersona);
            joyStick.SetActive(true);
            refPanelInterfazBotonesInterfazAfuera.ActivarPanel(true);
            sonidos.activarSalirConsola();
            if (mensajeIngresarSituacion)
                Destroy(mensajeIngresarSituacion);
        }
        
        #endregion
    }

    public enum SituacionSeleccionada
    {
        recta,
        parabola,
        circunferencia,
        elipse,
        hiperbola
    }
}