﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;
using TMPro;

namespace NSInterfazAvanzada
{
    public class PanelInterfazMensaje : AbstractPanelInterfaz
    {
        #region members
        public TextMeshProUGUI textMensaje;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake()
        {

        }

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion

        #region private methods

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }


        public void MtdActivarMnesaje(string Mensaje ) {
            textMensaje.text = Mensaje;
            ActivarPanel(true);
        }


        #endregion

        #region courutines

        #endregion
    }
}