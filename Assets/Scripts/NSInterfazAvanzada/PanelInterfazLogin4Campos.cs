﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI ;
using NsSeguridad;
using NSBoxMessage;
using NSInterfazAvanzada;
using NSTraduccionIdiomas;
using System;

namespace NSInterfazAvanzada 
{
    public class PanelInterfazLogin4Campos : AbstractPanelInterfaz
    {

        #region members
        public DiccionarioIdiomas Dic;
        public InputField inputNombre;
        public InputField inputCurso;
        public InputField inputIdCurso;
        public InputField inputInstituto;
        private ClsSeguridad seguridad;
        private string[] datosLti;
        [SerializeField]
        private BoxMessageManager refBoxMessageManager;


        private string[] cmd;
        private string correoMOno;
        private string nombreMono;
        private string instituMono;


        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake()
        {
            seguridad = GameObject.FindGameObjectWithTag("Seguridad").GetComponent<ClsSeguridad>();
            Dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }

        void Start()
        {
            seguridad.DlResLoginAula = loguin;

            inputNombre.text = Dic.Traducir("TextPlaceholderLoguin", "");
            inputNombre.placeholder.GetComponent<Text>().text = Dic.Traducir("TextPlaceholderLoguin", ""); ;

            inputCurso.text = Dic.Traducir("TextPlaceHolderCurso", "");
            inputCurso.placeholder.GetComponent<Text>().text = Dic.Traducir("TextPlaceHolderCurso", "");

            inputIdCurso.text = Dic.Traducir("TextPlaceholderIDCurso", "");
            inputIdCurso.placeholder.GetComponent<Text>().text = Dic.Traducir("TextPlaceholderIDCurso", "");

            inputInstituto.text = Dic.Traducir("TextPlaceholderInstitucion", "");
            inputInstituto.placeholder.GetComponent<Text>().text = Dic.Traducir("TextPlaceholderInstitucion", "");

            datosLti = seguridad.GetDatosSesion();
            if(seguridad.LtiActivo){
                inputNombre.interactable=false;
                inputCurso.interactable = false;
                inputIdCurso.interactable = false;
                inputInstituto.interactable = false;
            }

            if (seguridad.modoMonoUsuario)
            {

#if UNITY_EDITOR || UNITY_STANDALONE
                // Obtenemos los argumentos enviados a la aplicación de escritorio.
                cmd = System.Environment.CommandLine.Split(',');
                try
                {
                    if (cmd.Length > 5)
                    {
                        nombreMono = cmd[3];
                        instituMono = cmd[4];
                        correoMOno = cmd[5];

                    }

                }
                catch
                {
                    refBoxMessageManager.MtdCreateBoxMessageInfo("esto es lo que hay dentro de la cmd " + cmd.ToString(), "ACEPTAR");
                }
#elif UNITY_ANDROID || UNITY_IPHONE
            try{
						AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
						AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
						if (currentActivity != null)
						{
                            AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
                            if (intent != null)
                                {
                                    nombreMono = safeCallStringMethod(intent, "getStringExtra", "nombre");
                                    instituMono = safeCallStringMethod(intent, "getStringExtra", "institucion");
                                    correoMOno = safeCallStringMethod(intent, "getStringExtra", "correo");
                                }
                        }

				}catch (Exception e)
                {
					Debug.Log(e.ToString());
				}

#endif
            }


        }

        private void Update()
        {

#if UNITY_WEBGL

            if (seguridad.LtiActivo){
                    inputNombre.text = datosLti[0];
                    inputCurso.text = datosLti[1];
                    inputIdCurso.text = datosLti[2];
                    inputInstituto.text = datosLti[3];
                }

#elif UNITY_ANDROID || UNITY_IPHONE|| UNITY_EDITOR || UNITY_STANDALONE

            if (seguridad.modoMonoUsuario)
            {
                inputNombre.text = nombreMono;

                inputInstituto.text = instituMono;

            }
#endif

        }


        #endregion

        #region static methods

        public static string safeCallStringMethod(AndroidJavaObject javaObject, string methodName, params object[] args)
        {

				if (args == null) args = new object[] { null };
				IntPtr methodID = AndroidJNIHelper.GetMethodID<string>(javaObject.GetRawClass(), methodName, args, false);
				jvalue[] jniArgs = AndroidJNIHelper.CreateJNIArgArray(args);
				try
				{
					IntPtr returnValue = AndroidJNI.CallObjectMethod(javaObject.GetRawObject(), methodID, jniArgs);
					if (IntPtr.Zero != returnValue)
					{
						var val = AndroidJNI.GetStringUTFChars(returnValue);
						AndroidJNI.DeleteLocalRef(returnValue);
						return val;
					}
				}
				finally
				{
					AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
				}

				return null;

        }

        #endregion

        #region public methods

        public void loguin(int op) {
            if (op == 0)
                ActivarPanel(false);
        }

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        /// <summary>
        /// metodo que activa el inicio de sesion offline
        /// </summary>
        public void BtnIniciarOffline()
        {

            if (inputNombre.text != "" && inputCurso.text != "" && inputInstituto.text != "" && inputIdCurso.text != "")
            {
                Debug.Log("entre a llamar aula loginRequest");
                seguridad.MtdLoguinOffLine(inputNombre.text, inputCurso.text, inputIdCurso.text, inputInstituto.text);
            }
            else
            {
                refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeLLenarcampos", "Todos los campos son requeridos.."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
            }

        }



#endregion

#region courutines

#endregion
    }
}