﻿using NSEvaluacion;
using NsSeguridad;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{
    public class PanelInterfazInformacionUsuario : AbstractPanelInterfaz
    {
        #region members

        [SerializeField]
        private Animator animatorBarraInformacionUsuario;

        [SerializeField]
        private ControladorDatosSesion refControladorDatosSesion;

        [SerializeField]
        private Text textTiempoSesion;

        [SerializeField]
        private Text textCantidadIntentos;

        [SerializeField]
        private Text textNombreUsuario;

        [SerializeField]
        private ClsSeguridad refClsSeguridad;
        #endregion

        #region accesors

        #endregion

        #region events

        #endregion

        #region monoBehaviour

        void Awake()
        {
            refControladorDatosSesion._listenerCantidadIntentos += ActualizarValorCantidadIntentos;
            refControladorDatosSesion._listenerTiempoSituacion += ActualizarValorTiempoSesion;

            var tmpDatosConexion = refClsSeguridad.GetDatosSesion();
            ActualizarValorNombreUsuario(tmpDatosConexion[0]);
        }

        private void OnEnable()
        {
            refControladorDatosSesion.IniciarNuevaSesionSituacion();
        }
        #endregion

        #region private methods

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        public void OnButtonDesplegarContraerMenu()
        {
            animatorBarraInformacionUsuario.SetBool("desplegar", !animatorBarraInformacionUsuario.GetBool("desplegar"));
        }

        public void ActualizarValorTiempoSesion(float argTiempoSesionActual)
        {
            var tmpSegundos = Mathf.Floor(argTiempoSesionActual % 60);
            var tmpMinutos = Mathf.Floor(argTiempoSesionActual / 60);
            textTiempoSesion.text = ((tmpMinutos < 10f) ? "0" + tmpMinutos.ToString() : tmpMinutos.ToString()) + ":" + ((tmpSegundos < 10f) ? "0" + tmpSegundos.ToString() : tmpSegundos.ToString());
        }

        public void ActualizarValorCantidadIntentos(byte argCantidadIntentosActuales)
        {
            if (textCantidadIntentos)         
                textCantidadIntentos.text = argCantidadIntentosActuales.ToString();
        }

        public void ActualizarValorNombreUsuario(string argNombreUsuarioActual)
        {
            textNombreUsuario.text = argNombreUsuarioActual;
        }
        #endregion

        #region courutines

        #endregion
    }
}