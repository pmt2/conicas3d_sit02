﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSInterfazAvanzada 
{
	public class PanelInterfazAyuda : AbstractPanelInterfaz
	{

		#region members

 		#endregion

 		#region accesors

 		#endregion

 		#region events

 		#endregion

 		#region monoBehaviour

 		void Awake ()
 		{
		
		}

 		// Use this for initialization
		void Start ()
		{

		}

		// Update is called once per frame
 		void Update ()
		{

		}

        #endregion

        #region private methods

        #endregion

        #region public methods
        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }
        #endregion

        #region courutines

        #endregion
    }
}