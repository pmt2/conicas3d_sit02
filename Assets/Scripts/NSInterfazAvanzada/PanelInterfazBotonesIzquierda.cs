﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NSInterfazAvanzada
{
	public class PanelInterfazBotonesIzquierda : AbstractPanelInterfaz
	{

        #region members
        [SerializeField]
        private Button buttonSeleccionFuncion;

        [SerializeField]
        private Button buttonRegistroDatos;

 		#endregion

 		#region accesors

 		#endregion

 		#region events

 		#endregion

 		#region monoBehaviour
        private void OnEnable()
        {
            buttonRegistroDatos.interactable = false;
            ActivarHaloVerde(buttonSeleccionFuncion, false);
            ActivarHaloVerde(buttonRegistroDatos, false);
            ActivarHaloRojo(buttonSeleccionFuncion, false);
            ActivarHaloRojo(buttonRegistroDatos, false);
        }

        #endregion

        #region private methods

        private void ActivarHaloVerde(Button argButton, bool argActivar)
        {
            argButton.transform.GetChild(0).gameObject.SetActive(argActivar);
        }

        private void ActivarHaloRojo(Button argButton, bool argActivar)
        {
            buttonSeleccionFuncion.transform.GetChild(1).gameObject.SetActive(argActivar);
        }

        #endregion

        #region public methods

        public override void ActivarPanel(bool argActivar = true)
        {
            Mostrar(argActivar);
        }

        public void SetSeleccionYDatosFuncionCorrecta(bool argCorrecta)
        {
            if (argCorrecta)
            {
                ActivarHaloVerde(buttonSeleccionFuncion, true);
                ActivarHaloRojo(buttonSeleccionFuncion, false);
            }
            else
            {
                ActivarHaloVerde(buttonSeleccionFuncion, false);
                ActivarHaloRojo(buttonSeleccionFuncion, true);
            }

            buttonRegistroDatos.interactable = true;
        }

        public void SetRegistroDatosCorrectos(bool argCorrectos)
        {
            if (argCorrectos)
            {
                ActivarHaloVerde(buttonRegistroDatos, true);
                ActivarHaloRojo(buttonRegistroDatos, false);
            }
            else
            {
                ActivarHaloVerde(buttonRegistroDatos, false);
                ActivarHaloRojo(buttonRegistroDatos, true);
            }
        }
        #endregion

        #region courutines

        #endregion
    }
}