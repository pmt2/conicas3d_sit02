﻿using NSBoxMessage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSTraduccionIdiomas;

namespace NSCreacionPDF
{
    /// <summary>
    /// Clase que contiene todos los textos UI y objetos UI del PDF que necesitan asignacion de valores con respecto a lo que sucedio en el simulador en la segunda sala
    /// </summary>
    public class ControladorValoresPDFSegundaSala : MonoBehaviour
    {
        #region members

        [SerializeField, Header("Valores y datos del usuario"), Space(10)]
        private Text textUsuarioValor;

        [SerializeField]
        private Text textInstitucionValor;

        [SerializeField]
        private Text textSituacionValor;

        [SerializeField]
        private Text textCursoValor;

        [SerializeField]
        private Text textUnidadValor;

        [SerializeField]
        private Text textIDCursoValor;

        [SerializeField]
        private Text textFechaInicioValor;

        [SerializeField]
        private Text textTiempoPracticaValor;

        [SerializeField]
        private Text textIntentosValor;

        [SerializeField]
        private Text textCalificacionValor;
    
        /// <summary>
        /// Array de todas las raw textures que contendran las capturas de los cortes 
        /// </summary>
        [SerializeField, Header("Captura corte cono")]
        private RawImage[] capturasCorteConicas;

        private int cantidadCapturasActuales;

        [SerializeField]
        private BoxMessageManager refBoxMessageManager;

        private DiccionarioIdiomas dicIdiomas;
        #endregion
        #region  MonoBehaviour

        private void Awake()
        {
            dicIdiomas = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }

        #endregion

        #region public methods

        public void SetUsuario(string argUsuario)
        {
            textUsuarioValor.text = argUsuario;
        }

        public void SetInstitucion(string argInstitucion)
        {
            textInstitucionValor.text = argInstitucion;
        }

        public void SetSituacion(string argSituacion)
        {
            textSituacionValor.text = argSituacion;
        }

        public void SetCurso(string argCurso)
        {
            textCursoValor.text = argCurso;
        }

        public void SetUnidad(string argUnidad)
        {
            textUnidadValor.text = argUnidad;
        }

        public void SetIDCurso(string argIDCurso)
        {
            textIDCursoValor.text = argIDCurso;
        }

        public void SetFechaInicio(string argFechaInicio)
        {
            textFechaInicioValor.text = argFechaInicio;
        }

        public void SetTiempoPractica(string argTiempoPractica)
        {
            textTiempoPracticaValor.text = argTiempoPractica;
        }

        public void SetIntentos(string argIntentos)
        {
            textIntentosValor.text = argIntentos;
        }

        public void SetCalificacion(string argCalificacion)
        {
            textCalificacionValor.text = argCalificacion;
        }

        public void AddCapturaCorteConicas(Texture2D argTexture)
        {
            if (cantidadCapturasActuales < capturasCorteConicas.Length)
            {
                capturasCorteConicas[cantidadCapturasActuales].texture = argTexture;
                cantidadCapturasActuales++;
                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajeTomarfoto", "Has tomado una imagen de tu corte."), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
            }
            else
            {
                refBoxMessageManager.MtdCreateBoxMessageInfo(dicIdiomas.Traducir("mensajesLimiteDeFotosAlcanzado", "Limite de capturas alcanzado"), dicIdiomas.Traducir("TextBotonAceptar", "ACEPTAR"));
                cantidadCapturasActuales= 0; ;
            }
        }

        #endregion
    }
}