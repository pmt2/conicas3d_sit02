﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfazAvanzada;

public class PanelInterfazSegundaSala : AbstractPanelInterfaz
{


    #region members


    public GameObject panelDeInfromacionActivo;


    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour

    void Awake()
    {

    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    #endregion

    #region private methods

    #endregion

    #region public methods

    public override void ActivarPanel(bool argActivar = true)
    {
        Mostrar(argActivar);
    }


    public void MtdActivarInfoSituacionDeLaSituacionActual()
    {
        panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarSituacion();
    }

    public void MtdActivarInfoEcuacionesDeLaSituacionActual()
    {
        panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarEcuaciones();
    }

    public void MtdActivarInfoProcedimientoDeLaSituacionActual()
    {
        panelDeInfromacionActivo.GetComponent<clsInformacion>().MtdActivarProcedimeinto();
    }

    #endregion

    #region courutines

    #endregion

}
