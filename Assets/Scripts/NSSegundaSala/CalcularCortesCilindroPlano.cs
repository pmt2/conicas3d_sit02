﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NSSegundaSala
{
    public class CalcularCortesCilindroPlano : MonoBehaviour
    {
        [SerializeField]
        private Transform cilindro;

        [SerializeField]
        private Transform plano;

        [SerializeField]
        private Transform puntoCollider;

        private void FixedUpdate()
        {
            CacularPosicionPuntos();
        }

        private void CacularPosicionPuntos()
        {
            var tmpPlaneCollision = new Plane(plano.forward, plano.position);

            var tmpRayUp = new Ray(cilindro.position, cilindro.up * 0.5f * cilindro.localScale.magnitude);
            var tmpRayDown = new Ray(cilindro.position, -cilindro.up * 0.5f * cilindro.localScale.magnitude);
            var tmpRayDistanceHit = Mathf.Infinity;

            if (tmpPlaneCollision.Raycast(tmpRayUp, out tmpRayDistanceHit))
            {
                if (tmpRayDistanceHit <= 0.5f * cilindro.localScale.magnitude)
                    puntoCollider.position = cilindro.position + cilindro.up * tmpRayDistanceHit;
                else
                    puntoCollider.position = tmpPlaneCollision.ClosestPointOnPlane(cilindro.position + cilindro.up * 0.5f * cilindro.localScale.magnitude);
            }
            else if (tmpPlaneCollision.Raycast(tmpRayDown, out tmpRayDistanceHit))
            {
                if (tmpRayDistanceHit <= 0.5f * cilindro.localScale.magnitude)
                    puntoCollider.position = cilindro.position - cilindro.up * tmpRayDistanceHit;
                else
                    puntoCollider.position = tmpPlaneCollision.ClosestPointOnPlane(cilindro.position - cilindro.up * 0.5f * cilindro.localScale.magnitude);
            }
            else
            {
                var tmpClosestPointOnPlane = tmpPlaneCollision.ClosestPointOnPlane(cilindro.position);
                puntoCollider.position = tmpClosestPointOnPlane;
            }
        }
    }
}