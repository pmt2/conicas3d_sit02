﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClsPdfSegundaSala : MonoBehaviour {

    #region members

    public GameObject hoja1SalaDos;
    public GameObject Hoja2SalaDos;

    public  Camera Camaradelplano;
    public Camera Camaradelcono;


    public RawImage[] RawImageDelPdfSegundaSala;

    public RawImage ImagenDelCono;
    public RawImage ImagenMapa;


    private RenderTexture renderTexturePDF;
    private List<Texture2D> FotosTomadas;
    #endregion


    #region MonoBehaviour
    // Use this for initialization
    void Start () {

        FotosTomadas = new List<Texture2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
       
	}
    #endregion


    #region private methods


    #endregion


    #region public methods
    /// <summary>
    /// toma una una foto de los dos sistemaes muy comico 
    /// </summary>
    public IEnumerator MtdTomarImgen()
    {

        //var tmpGameObjectHojaPdfActual = transform.GetChild().gameObject;
        //tmpGameObjectHojaPdfActual.SetActive(true);
        yield return new WaitForEndOfFrame();
        Camaradelplano.Render();
        RenderTexture.active = renderTexturePDF;
        var tmpTexture2D = new Texture2D(824, 1080, TextureFormat.ARGB32, false, true);
        tmpTexture2D.ReadPixels(new Rect(548, 0, 824, 1080), 0, 0);
        tmpTexture2D.Apply();

        FotosTomadas.Add(tmpTexture2D);
        RenderTexture.active = null;
       // tmpGameObjectHojaPdfActual.SetActive(false);


        RawImageDelPdfSegundaSala[1] = ImagenMapa;
    }

    #endregion

}
