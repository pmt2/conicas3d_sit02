﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NSInterfazAvanzada;

public class clsPuntoParaNota : MonoBehaviour
{


    #region members
    /// <summary>
    /// prefabricado Nota 
    /// </summary>
    public GameObject prefabricadoDeNota;
    /// <summary>
    /// camara activa mientras se manipula el plano
    /// </summary>
    public Camera camaraAcual;

    private GameObject listadeNotas;
    private GameObject MiNota;
    private bool activoExitmouse;

    #endregion

    #region accesors

    #endregion

    #region events

    #endregion

    #region monoBehaviour

    void Awake()
    {
        listadeNotas = GameObject.FindGameObjectWithTag("ListaNotas");
    }

    void Start()
    {
        Debug.Log(this.transform.position);
        MtdInstanciarNota();
    }
    #endregion

    #region private methods



    #endregion


    #region public methods

    /// <summary>
    /// activa la nota al instanciar 
    /// </summary>
    public void MtdInstanciarNota()
    {
        float z = transform.position.z-0.3f;

        var PosMiNota = camaraAcual.WorldToScreenPoint(new Vector3(transform.position.x, transform.position.y, z));
        if (transform.position.z<=7.5)
                PosMiNota = camaraAcual.WorldToScreenPoint(transform.position);

        MiNota = Instantiate(prefabricadoDeNota, PosMiNota, Quaternion.identity, listadeNotas.transform);
        MiNota.SetActive(false);

        MiNota.GetComponent<ClsNota>().AgregarMetodoEliminar(mtdCerrar);
        MiNota.GetComponent<ClsNota>().AgregarMetodoClick(mtdActivarModoExitEnNota);
        MiNota.GetComponent<ClsNota>().ActivarPanel();

    }
    /// <summary>
    /// Destrulle los objetos miNota y punto 
    /// </summary>
    public void mtdCerrar()
    {
        Destroy(MiNota);
        Destroy(gameObject);

    }

    private void OnMouseEnter()
    {

        if (activoExitmouse)
        {
            MiNota.GetComponent<ClsNota>().ActivarPanel();
            MiNota.GetComponent<ClsNota>().Aceptar.gameObject.SetActive(false);
            MiNota.GetComponent<ClsNota>().Eliminar.gameObject.SetActive(false);
        }
    }

    private void OnMouseExit()
    {

        if (activoExitmouse)
        {
            MiNota.GetComponent<ClsNota>().ActivarPanel(false);
        }

    }


    private void OnMouseDown()
    {

        if (Input.GetMouseButton(0))
        {
            Ray ray = camaraAcual.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {

                activoExitmouse = false;
                MiNota.GetComponent<ClsNota>().Aceptar.gameObject.SetActive(true);
                MiNota.GetComponent<ClsNota>().Eliminar.gameObject.SetActive(true);
                MiNota.GetComponent<ClsNota>().ActivarPanel(true);


            }

        }

    }


    public void mtdActivarModoExitEnNota()
    {
        activoExitmouse = true;
    }

    #endregion

    #region courutines

    #endregion

}
