﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clsLaser : MonoBehaviour {

    # region members

    public Toggle botonLaser;
    public GameObject ObjetoEjex;
    public GameObject ObjetoEjey;
    public GameObject ObjetoCordenadas;

    public bool LaseAcctivo;



    #region

    #endregion MonoBehaviour
    private void Start()
    {
        LaseAcctivo = gameObject.activeSelf;


    }

    private void Update()
    {
        if (!ObjetoCordenadas.activeSelf)
        {
            ObjetoCordenadas.SetActive(true);
        }
    }
    #endregion

    #region  public methods
    public void mtdActivarLaser(bool Activar) {

        if (Activar)
        {
            ObjetoEjex.SetActive(Activar);

            ObjetoEjey.SetActive(Activar);

            ObjetoCordenadas.SetActive(Activar);

        }
        else {

            ObjetoEjex.transform.localPosition = new Vector3(0, ObjetoEjex.transform.position.y, 0);
            ObjetoEjey.transform.localPosition = new Vector3(0, ObjetoEjey.transform.position.y, 0);
           // ObjetoCordenadas.transform.localPosition = new Vector3(0, ObjetoCordenadas.transform.position.y, 0);
           // ObjetoCordenadas.GetComponent<clsCordenada>().mtdResetValoresCordenadas();
            ObjetoEjex.SetActive(Activar);
            ObjetoEjey.SetActive(Activar);
            ObjetoCordenadas.SetActive(Activar);

        }

        LaseAcctivo = Activar;
    }

    public void mtdActivar() {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        else {
            gameObject.SetActive(true);
        }
    }

    public void mtdresetLaser()
    {
        gameObject.SetActive(false);
    }

    public void DesactivarBoton()
    {
        botonLaser.isOn = false;
        gameObject.SetActive(false);
    }

    #endregion


}
