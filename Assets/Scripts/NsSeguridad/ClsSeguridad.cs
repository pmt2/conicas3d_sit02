﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using SimpleJSON;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using System.Net.NetworkInformation;
using NSInterfazAvanzada;
using NSCreacionPDF;
using System.Linq;
using NSBoxMessage;
using System.Runtime.InteropServices;
using NSTraduccionIdiomas;
#if !UNITY_WEBGL
using ValidacionMenu;
#endif

namespace NsSeguridad
{
    public class ClsSeguridad : MonoBehaviour
    {

        #region members
        public PanelInterfazMensaje PanelMensaje;
        /// <summary>
        /// hace referencia al panelcon 2 campos de loguin
        /// </summary>
        public PanelInterfazLogin2Campos PanelModoAula;
        /// <summary>
        /// hace referencia al panel con 4 para el loguin
        /// </summary>
        public PanelInterfazLogin4Campos PanelOffline;

        public GameObject analogo;

        public bool coneccionActiva = false;
        private string license_number = "";
        public bool modoMonoUsuario;
        public bool SeguridadSwitch= false;
        public bool ModoAula;
        private bool var_seguridad;
        private bool var_aula;
        private float timeOut = 2;

        private string licencia_url;
        /// <summary>
        /// campos usuario con valores que el usuario a ingresa
        /// </summary>
        public string usuario;
        /// <summary>
        /// campo curso que contiene el valor de la contraseña ingresada por el usuario
        /// </summary>
        public string curso;

        private string aula_name;
        private string aula_last_name;
        private string aula_class_id;
        private string aula_curso;
        private string aula_school_name;
        public string Lti_datos;

        /// <summary>
        /// este campos simplemente es el nombre completo del usuario (nombre + apellido para cuando se loguea contra el servidor)
        /// </summary>
        private string Aula_usuario;

        public string url_aula = "http://190.84.230.23:3000";

        public string url_score_lti = "https://lti.servercloudlabs.com/lti_launcher/setScore";
        public string valid_lti_url = "lti.servercloudlabs.com";
        public bool LtiActivo;

        //web Aula
        [DllImport("__Internal")]
        private static extern bool isMobile();
        public string webGLCode = "";
        public string WebGLUrl = "mycloudlabs_global";

        public float maxScore = 5f;



        private string fileName = "";
        [SerializeField]
        private BoxMessageManager refBoxMessageManager;
        #endregion

        #region Delegate
        public delegate void DelegatetMensaje(string mensage);
        /// <summary>
        ///  delegado el cual se llamda a la hora de reportar un mesaje importante para el usuario, sele como parametro el mensaje a mostrar
        /// </summary>
        public DelegatetMensaje DltMensaje = delegate (string mensage) { };

        public delegate void DelegatetResLoginAula(int op);
        /// <summary>
        /// delegado el cual retorna la respues true si a sido aceptado el ingreso del usuario
        /// </summary>
        public DelegatetResLoginAula DlResLoginAula = delegate (int op) { };

        private DiccionarioIdiomas Dic;
        #endregion

        #region accesors

        public string _fileName
        {
            get
            {
                return fileName;
            }
        }

        #endregion

        #region  monoBehaviour
        private void Awake()
        {
            Dic = GameObject.FindGameObjectWithTag("DiccionarioIdiomas").GetComponent<DiccionarioIdiomas>();
        }
        // Use this for initialization
        void Start()
        {
            DltMensaje = PanelMensaje.MtdActivarMnesaje;
            //CargarExternalXML();
            /*if (LtiActivo)
                LtiNotificacion();*/

        }

        // Update is called once per frame
        void Update()
        {

        }

        #endregion

        #region public methods

        /// <summary>
        /// determina si el simulador comienza en modo online o offline
        /// </summary>
        public void mtdIniciarSimulador()
        {
#if !UNITY_WEBGL
            CargarExternalXML();
#endif
#if UNITY_WEBGL

            if(LtiActivo)
            {
                LtiNotificacion();
            }
            else if (ModoAula)
            {
                webaulaNotification();
            }


#endif
            //----------------------------------------------------------
            var_seguridad = SeguridadSwitch;
            var_aula = ModoAula;

            if (var_seguridad)
            {
                //getLicenseData();
#if !UNITY_WEBGL

                if (Validacion.Validar())
                {
                    
                    if (var_aula)
                    {
                        
                        PanelModoAula.ActivarPanel(true);

                    }
                    else
                    {
                        PanelOffline.ActivarPanel(true);


                    }
                }
                else
                {
                    //Alerta de seguridad
                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("TextBoxAlertSeguridad", ""), Dic.Traducir("TextAlertSeguridad", ""), cerrar);

                }
#endif
            }
            else
            {
                if (var_aula)
                {

                    PanelModoAula.ActivarPanel(true);

                }
                else
                {
                    PanelOffline.ActivarPanel(true);


                }
            }
        }

        /// <summary>
        /// metodo que resive las variables de secion 
        /// </summary>
        /// <param name="US">nobre de usuario </param>
        /// <param name="pass">contraseña </param>
        public void mtdLoguinAula(string US, string pass)
        {
            usuario = US;
            curso = pass;
            aulaLoginRequest();
        }

        /// <summary>
        /// metodo que resive los valores del loguin offline
        /// </summary>
        /// <param name="us">usuario</param>
        /// <param name="curso">curso</param>
        /// <param name="idCruso">identificador del curso</param>
        /// <param name="inst">intitucion</param>
        /// 
        public void MtdLoguinOffLine(string us, string curso, string idCruso, string inst)
        {
            aula_name = us;
            aula_last_name = "";
            aula_curso = Dic.Traducir("NombreCurso", "Matematícas");
            aula_class_id = idCruso;
            aula_school_name = inst;
            Aula_usuario = us;
            DlResLoginAula(0);
        }

        /// <summary>
        /// retorna un array con los datos de la sesion
        /// [0]= nombre del usuario , [1]curso ,[2] id curso o grupo , [3] nombre de la institucion, [4] Nombre de usuario completo (nombre + apellido cuando se loguea desde internet, cunado no es el mismo user) este es el que se coloca en el reporte como nombre   
        /// </summary>
        /// <returns></returns>
        public string[] GetDatosSesion()
        {
            string[] datos = { aula_name, aula_curso, aula_class_id, aula_school_name, Aula_usuario, };
            return datos;
        }


#endregion

#region courutines

        /// <summary>
        /// corrutina que verifica las diferentes opciones de la licencia y notifica de cual quier problema  
        /// </summary>
        /// <param name="www"></param>
        /// <returns></returns>
        IEnumerator WaitForRequest(WWW www)
        {
            Debug.Log("in waitforrequest");
            float timer = 0;
            bool failed = false;

            while (!www.isDone)
            {
                if (timer > timeOut)
                { failed = true; break; }
                timer += Time.deltaTime;
                yield return null;
            }

            if (failed)
            {
                www.Dispose();

                if (PlayerPrefs.HasKey("attempts"))
                {
                    int attempts = PlayerPrefs.GetInt("attempts");
                    if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                    {
                        //AbrirPanel();activa el panel de mensaje generico
                        //CerrarPanel();cierra el panle de mensaje generico
                        //panelSesion.SetActive(true); activa el panel de sesion loguin ofline
                        attempts = attempts + 1;
                        PlayerPrefs.SetInt("attempts", attempts);
                        PlayerPrefs.Save();

                        if (var_aula)
                        {
                            // panelSesion.SetActive(true); activa el panel de sesion ofline
                            activarModoAula();
                        }
                    }
                    else
                    {

                        refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeMuchoTiempo", "Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación."),Dic.Traducir("TextBotonAceptar", "ACEPTAR"),cerrar);


                    }
                }
                else
                {

                    refBoxMessageManager.MtdCreateBoxMessageDecision(Dic.Traducir("MensajeOPPs", "Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación."), Dic.Traducir("TextBotonVALIDAR", "VALIDAR"), Dic.Traducir("TextBotonCancelar", "CACELAR"), getLicenseData, cerrar);

                }
            }
            else
            {
                if (string.IsNullOrEmpty(www.error))
                {
                    Debug.Log("WWW Ok!: " + www.text);
                    var _data = JSON.Parse(www.text);

                    switch (_data["result"].Value)
                    {
                        case "error":
                            switch (_data["message_id"].AsInt)
                            {
                                case 1:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir ("MensajeNumeroLIcencia", "El número de licencia con el que se ha activado el producto es inválido. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"),cerrar);

                                    break;
                                case 2:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeLiccenciaSinActivar", "Su licencia aún no ha sido activada. Por favor comuníquese con su proveedor."),Dic.Traducir("TextBotonAceptar", "ACEPTAR"),cerrar);

                                    break;
                                case 3:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeLiccenciaSinActivar", "Su licencia aún no ha sido activada. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"),cerrar);

                                    break;
                                case 4:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeLicenciaCaducada", "Su licencia ha caducado. Por favor comuníquese con su proveedor."),Dic.Traducir( "TextBotonAceptar","ACEPTAR"),cerrar);

                                    break;
                                case 5:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeLicenciaNoPermite", "La licencia adquirida no permite ejecutar esta aplicación. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"), cerrar);

                                    break;
                                case 6:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }


                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeNumeroActivaciones", "Se ha excedido el número de activaciones permitidas por su licencia. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"), cerrar);

                                    break;
                                case 7:
                                    if (PlayerPrefs.HasKey("attempts"))
                                    {
                                        PlayerPrefs.SetInt("attempts", PlayerPrefs.GetInt("offline_attempts"));
                                        PlayerPrefs.Save();
                                    }

                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeResultadoInvalido", "El proceso de validación arrojó un resultado inválido. Por favor reinstale la aplicación e intente nuevamente."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"), cerrar);

                                    break;
                            }
                            break;
                        case "success":
                            PlayerPrefs.SetInt("attempts", 0);
                            PlayerPrefs.SetInt("offline_attempts", _data["offline_attempts"].AsInt);
                            PlayerPrefs.Save();

                            //CerrarPanel();
                            //panelSesion.SetActive(true); activa el panel de seccion loguin 

                            if (var_aula)
                            {
                                activarModoAula();

                            }
                            break;
                    }
                }
                else
                {

                    if (PlayerPrefs.HasKey("attempts"))
                    {
                        int attempts = PlayerPrefs.GetInt("attempts");
                        if (attempts < PlayerPrefs.GetInt("offline_attempts"))
                        {
                            // AbrirPanel(); abre panel de mensaje generico 
                            // CerrarPanel(); cierra el panel de mensaje generico 
                            //panelSesion.SetActive(true);activa el panel de loguin offline
                            attempts = attempts + 1;
                            PlayerPrefs.SetInt("attempts", attempts);
                            PlayerPrefs.Save();

                            if (var_aula)
                            {
                                //panelSesion.SetActive(true); activa panel de seccion ofline 
                                activarModoAula();

                            }
                        }
                        else
                        {
                            //AbrirPanel(); activa panel de mensaje

                            // botonesValidacion.SetActive(true); activa la botonera de validar secion o reintentar 
                            // btnValidar.SetActive(true);activa boton validar
                            //btnCancelar.SetActive(true);activa boton cancelar
                            // DltMensaje("Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación.");
                            refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("MensajeMuchoTiempo", "Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"), cerrar);
                            /*switch (IdiomaActual)
                            {
                                case "Ingles":
                                    Mensaje("It seems too much time has passed since the last time your license was verified. Please connect your device to the internet and click VALIDATE or CANCEL to close the application.");
                                    break;
                                case "Espanol":
                                    Mensaje("Parece que ha pasado mucho tiempo desde la última vez que su licencia fue verificada. Por favor conecte su dispositivo a Internet y haga clic en el botón VALIDAR o CANCELAR para cerrar la aplicación.");
                                    break;
                                case "Portugues":
                                    Mensaje("Parece que passou muito tempo desde a última vez que sua licença foi verificada. Por favor, conecte seu dispositivo à Internet e clique no botão VALIDAR ou CANCELAR para fechar o aplicativo.");
                                    break;
                            }*/

                        }
                    }
                    else
                    {
                        // AbrirPanel(); abre panel de mensaje generico 
                        // botonesValidacion.SetActive(true); activa el padre de los botones de validar y cancelar
                        //btnValidar.SetActive(true);ativa el boton validar
                        // btnCancelar.SetActive(true);activa el boton cancelar  
                        //DltMensaje("Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación.");
                        refBoxMessageManager.MtdCreateBoxMessageDecision(Dic.Traducir("MensajeOPPs", "Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación."), Dic.Traducir("TextBotonVALIDAR", "VALIDAR"), Dic.Traducir("TextBotonCancelar", "CACELAR"), getLicenseData, cerrar);
                        /*switch (IdiomaActual)
                        {
                            case "Ingles":
                                Mensaje("Oops, it seems there was a problem with the internet connection or the CloudLabs servers. Click VALIDATE to try again or CANCEL to close the application.");
                                break;
                            case "Espanol":
                                Mensaje("Opps, parece que algo ha fallado en la conexión a Internet o con los servidores CloudLabs. Presione el botón VALIDAR para intentarlo de nuevo o CANCELAR para cerrar la aplicación.");
                                break;
                            case "Portugues":
                                Mensaje("Parece que algo deu errado na conexão com a Internet ou com os servidores CloudLabs. Clique no botão VALIDAR para tentar outra vez ou CANCELAR para fechar o aplicativo.");
                                break;
                        }*/
                    }
                }
            }
        }

        /// <summary>
        /// corrutinas que mmaneja el logueo desde el servidor remoto
        /// </summary>
        /// <param name="www"></param>
        /// <returns></returns>
        IEnumerator WaitForRequestAula(WWW www)
        {

            //var _btnIniciar = FindObjectOfType<PanelSeguridad>().btnIniciar;
            //var _sesion_mensaje_error = FindObjectOfType<PanelSeguridad>().sesion_mensaje_error;
            //string _IdiomaActual = FindObjectOfType<PanelSeguridad>().IdiomaActual;

            yield return www;

            if (www.error == null)
            {
                byte[] decodedBytes = Convert.FromBase64String(www.text);
                string decodedText = Encoding.UTF8.GetString(decodedBytes);
                Debug.Log(decodedText);
                var _data = JSON.Parse(decodedText);

                if (_data["state"] != null)
                {
                    if (_data["state"].Value == "true")
                    {
                        if (_data["res_code"] != null)
                        {
                            switch (_data["res_code"].Value)
                            {
                                case "INVALID_USER_PASS":

                                    DlResLoginAula(0);
                                    //DltMensaje("El usuario y/o la contraseña son inválidas. Verifíquelas e intente nuevamente.");
                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajePassword", "El usuario y/o la contraseña son inválidas. Verifíquelas e intente nuevamente."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                                    /*
                                    switch (_IdiomaActual)
                                    {
                                        case "Ingles":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("The username and/or password are invalid. Please check and try again.");
                                            break;
                                        case "Espanol":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("El usuario y/o la contraseña son inválidas. Verifíquelas e intente nuevamente.");
                                            break;
                                        case "Portugues":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("O usuário e/ou a senha são inválidos. Verifique e tente outra vez.");
                                            break;
                                    }*/
                                    break;
                                case "LAB_NOT_ASSIGNED":

                                    DlResLoginAula(0);
                                    // DltMensaje("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");
                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeLAB_NOT_ASSIGNED", "El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                                    /*
                                    switch (_IdiomaActual)
                                    {
                                        case "Ingles":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("The laboratory is not assigned to the student. Please ask your teacher.");
                                            break;
                                        case "Espanol":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("El laboratorio no está asignado al estudiante. Por favor comuníquese con su profesor.");
                                            break;
                                        case "Portugues":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("O laboratório não está liberado para o estudante. Por favor, entre em contato com seu fornecedor.");
                                            break;
                                    }*/
                                    break;
                                case "LOGIN_OK":
                                    aula_name = _data["name"].Value;
                                    aula_last_name = _data["last_name"].Value;
                                    aula_class_id = _data["class_group"].Value;
                                    aula_school_name = _data["school_name"].Value;
                                    Aula_usuario = aula_name + " " + aula_last_name;
                                    aula_curso = Dic.Traducir("NombreCurso", "Matematícas");
                                    DlResLoginAula(1);
                                    analogo.SetActive(true);
                                    coneccionActiva = true;

                                    /// CerrarPanel(); cierra el panel par aenpesar la simulacion

                                    /*asigana  el nombre del cuso segun el didioma
                                    
                                    switch (_IdiomaActual)
                                    {
                                        case "Ingles":
                                            simulador.curso = "Natural sciences";
                                            break;
                                        case "Espanol":
                                            simulador.curso = "Ciencias naturales";
                                            break;
                                        case "Portugues":
                                            simulador.curso = "Ciências Naturais";
                                            break;
                                    }*/
                                    break;
                                case "DB_EXCEPTION":

                                    DlResLoginAula(0);
                                    // DltMensaje("Error en la base de datos.Por favor comuníquese con su proveedor.");
                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeErrorDB", "Error en la base de datos.Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                                    /*switch (_IdiomaActual)
                                    {
                                        case "Ingles":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("Database error. Please contact your provider.");
                                            break;
                                        case "Espanol":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("Error en la base de datos. Por favor comuníquese con su proveedor.");
                                            break;
                                        case "Portugues":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("Erro na base de dados. Por favor, entre em contato com seu fornecedor.");
                                            break;
                                    }*/

                                    break;
                                case "LICENSE_EXPIRED":
                                    DlResLoginAula(0);
                                    // DltMensaje("Hay un problema con la licencia del gestor de aula. Se ingresará en modo offline.");
                                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeProblemasLicenciaGestor", "Hay un problema con la licencia del gestor de aula. Se ingresará en modo offline."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"), CamBioModoOfline);
                                    /*
                                    switch (_IdiomaActual)
                                    {
                                        case "Ingles":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("There is a problem with your licence, will continue without connecting to the teacher's device.");
                                            break;
                                        case "Espanol":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("Hay un problema con la licencia del gestor de aula. Se ingresará en modo offline.");
                                            break;
                                        case "Portugues":
                                            FindObjectOfType<PanelSeguridad>().Mensaje("Há um problema com a licença do gestor da sala. O ingresso será no modo offline.");
                                            break;
                                    }*/

                                    break;
                            }
                        }
                        else
                        {
                            DlResLoginAula(0);
                            // DltMensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                            refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeDelServidorRespuestaInvalidar", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                            /* switch (_IdiomaActual)
                             {
                                 case "Ingles":
                                     FindObjectOfType<PanelSeguridad>().Mensaje("The server response is invalid. Please contact your provider.");
                                     break;
                                 case "Espanol":
                                     FindObjectOfType<PanelSeguridad>().Mensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                                     break;
                                 case "Portugues":
                                     FindObjectOfType<PanelSeguridad>().Mensaje("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                                     break;
                             }*/
                        }
                    }
                    else
                    {
                        DlResLoginAula(0);
                        // DltMensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                        refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                        /*switch (_IdiomaActual)
                        {
                            case "Ingles":
                                FindObjectOfType<PanelSeguridad>().Mensaje("The server response is invalid. Please contact your provider.");
                                break;
                            case "Espanol":
                                FindObjectOfType<PanelSeguridad>().Mensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                                break;
                            case "Portugues":
                                FindObjectOfType<PanelSeguridad>().Mensaje("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                                break;
                        }*/
                    }
                }
                else
                {
                    DlResLoginAula(0);
                    // DltMensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                    refBoxMessageManager.MtdCreateBoxMessageInfo(Dic.Traducir("mensajeDelServidorRespuestaInvalida", "La respuesta del servidor es inválida. Por favor comuníquese con su proveedor."), Dic.Traducir("TextBotonAceptar", "ACEPTAR"));
                    /*switch (_IdiomaActual)
                    {
                        case "Ingles":
                            FindObjectOfType<PanelSeguridad>().Mensaje("The server response is invalid. Please contact your provider.");
                            break;
                        case "Espanol":
                            FindObjectOfType<PanelSeguridad>().Mensaje("La respuesta del servidor es inválida. Por favor comuníquese con su proveedor.");
                            break;
                        case "Portugues":
                            FindObjectOfType<PanelSeguridad>().Mensaje("A resposta do servidor é inválida. Por favor, entre em contato com o seu fornecedor.");
                            break;
                    }*/
                }

            }
            else
            {
                DlResLoginAula(2);
                //DltMensaje("No se ha podido establecer la conexión con el equipo del profesor. Haga clic en REINTENTAR para tratar de realizar la conexión nuevamente o haga clic en OFFLINE para continuar sin conectarse al equipo profesor.");
                refBoxMessageManager.MtdCreateBoxMessageDecision(Dic.Traducir("mensajeReIntentar", "No se ha podido establecer la conexión con el equipo del profesor. Haga clic en REINTENTAR para tratar de realizar la conexión nuevamente o haga clic en OFFLINE para continuar sin conectarse al equipo profesor.."), Dic.Traducir("TextBotonReintentar", "REINTENTAR"), Dic.Traducir("TextBotonOffline", "OFFLINE"), aulaLoginRequest, CamBioModoOfline);
                /*switch (_IdiomaActual)
                {
                    case "Ingles":
                        FindObjectOfType<PanelSeguridad>().Mensaje("It was not possible to connect to the teacher's device. Click RETRY to try to connect again or click OFFLINE to continue without connecting to the teacher's device.");
                        break;
                    case "Espanol":
                        FindObjectOfType<PanelSeguridad>().Mensaje("No se ha podido establecer la conexión con el equipo del profesor. Haga clic en REINTENTAR para tratar de realizar la conexión nuevamente o haga clic en OFFLINE para continuar sin conectarse al equipo profesor.");
                        break;
                    case "Portugues":
                        FindObjectOfType<PanelSeguridad>().Mensaje("Não foi possível estabelecer conexão com o equipamento do professor. Clique em OUTRA VEZ para tentar realizar a conexão novamente ou clique em OFFLINE para continuar sem se conectar com o equipamento do professor.");
                        break;
                }*/
            }
        }

#endregion

#region Private methods

        private void activarModoAula()
        {
            if (ModoAula)
            {
                PanelModoAula.ActivarPanel(true);
            }
            else {
                PanelOffline.ActivarPanel(true);
            }
        }

        /// <summary>
        /// inicia todo el proceso de obtencio de licencia
        /// </summary>
        private void getLicenseData()
        {

            string bundle_id = "com.cloudlabs.seccionesconicas";
            string device_id = "";
            string cmdInfo = "";


            if (Application.platform == RuntimePlatform.Android)
            {
#if UNITY_ANDROID
                    using (AndroidJavaClass jc = new AndroidJavaClass("com.cloudlabs.seccionesconicas"))
                    {
                        cmdInfo = jc.CallStatic<string>("getLauncherURL");
                        device_id = jc.CallStatic<string>("getDeviceData");
                    }
#endif
                if (cmdInfo != "no input data" && cmdInfo != "")
                {
                    license_number = cmdInfo.Substring(cmdInfo.IndexOf("LICENCIA=") + 9, 23);
                    bundle_id = cmdInfo.Substring(0, cmdInfo.IndexOf("://"));
                }
                else
                {
                    license_number = "";
                    //license_number = "53392-F5BD8-F445C-EB286";
                }
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
            {

                List<string> macsArray = new List<string>();
                NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface adapter in nics)
                {
                    PhysicalAddress address = adapter.GetPhysicalAddress();
                    byte[] bytes = address.GetAddressBytes();
                    string mac = null;
                    for (int i = 0; i < bytes.Length; i++)
                    {
                        mac = string.Concat(mac + (string.Format("{0}", bytes[i].ToString("X2"))));
                        if (i != bytes.Length - 1)
                        {
                            mac = string.Concat(mac + "-");
                        }
                    }
                    if (mac != null)
                    {
                        macsArray.Add(mac);
                    }
                }

                device_id = string.Join(",", macsArray.ToArray());

                string[] args = Environment.GetCommandLineArgs();

                if (args.Length > 1)
                {
                    license_number = args[1];

                }
            }


            if (license_number != "")
            {
                if (PlayerPrefs.HasKey("attempts"))
                {
                    licencia_url = "https://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(license_number) + "&primera_vez=false";
                }
                else
                {
                    licencia_url = "https://ielicenseserver.herokuapp.com/validacion/verificacion_licencia?dispositivo_id=" + WWW.EscapeURL(device_id) + "&bundle_id=" + WWW.EscapeURL(bundle_id) + "&licencia=" + WWW.EscapeURL(license_number) + "&primera_vez=true";
                }

                Debug.Log(licencia_url);
                WWW www = new WWW(licencia_url);
                StartCoroutine(WaitForRequest(www));
            }
            else
            {

                refBoxMessageManager.MtdCreateBoxMessageInfo("No ha sido posible comprobar su número de licencia. Asegúrese que esté abriendo esta aplicación desde el menú principal CloudLabs.", "ACEPTAR");

            }

        }

        private void LtiNotificacion() {

            if (Application.platform == RuntimePlatform.WebGLPlayer && LtiActivo == true && ModoAula==false)
            {
                if (!Application.absoluteURL.Contains(valid_lti_url))
                {
                    refBoxMessageManager.MtdCreateBoxMessageMini(Dic.Traducir("MensajeUrlInvalida", "La aplicación se abrió desde una ruta inválida. Asegúrese que se abra desde el curso respectivo."));


                }
                else
                {
                    string[] _parametrosLTI = Application.absoluteURL.Split('?');

                    if (_parametrosLTI.Length > 1)
                    {
                        string[] arrayParametros = _parametrosLTI[1].Split('&');

                        aula_name = WWW.UnEscapeURL(arrayParametros[0]);
                        aula_last_name = "";
                        aula_class_id = WWW.UnEscapeURL(arrayParametros[1]);
                        aula_curso = WWW.UnEscapeURL(arrayParametros[2]); ;
                        aula_school_name = WWW.UnEscapeURL(arrayParametros[3]);
                        Lti_datos = WWW.UnEscapeURL(arrayParametros[4]);
                        Aula_usuario = aula_name;

                        //DlResLoginAula(1);
                        //analogo.SetActive(true);
                        coneccionActiva = true;
                    }
                    activarModoAula();

                }
            }
        }

        //webAula
        private void webaulaNotification()
        {

            if (Application.platform == RuntimePlatform.WebGLPlayer && LtiActivo == false && ModoAula == true)
            {
                activarModoAula();
            }
        }


        private void Mensaje(string mensaje)
        {
            Debug.Log("respuesta de corrutina" + mensaje);
        }


#if UNITY_ANDROID
	   private void ProcessDirectory(DirectoryInfo aDir)
	    {
		    var files = aDir.GetFiles().Where(f => f.Extension == ".xml").ToArray();
		    foreach(var _fileName in files)
		    {
			    if(_fileName.Name.Equals("aula_conf.xml")){
				    fileName = _fileName.FullName;
				    break;
			    }
		    }
		
		    if (fileName.Equals("")) {
			    //var subDirs = aDir.GetDirectories ();
			
			    //foreach(var subDir in subDirs)
			    //{
			    //	ProcessDirectory(subDir);
			    //}
			    //GameObject.FindObjectOfType<PanelSeguridad> ().checkOptionLang ();
		    }
		    else {
			    openXML();
		    }
	    }
#endif

        /// <summary>
        ///  abre el archivo xml y extra la url y la variable aula
        /// </summary>
        private void openXML()
        {
            try
            {
                XmlDocument newXml = new XmlDocument();
                newXml.Load(fileName);

                var tmpNodoUrlAula = newXml.GetElementsByTagName("url_aula");
                url_aula = tmpNodoUrlAula[0].InnerText;

                var tmpNodoModoAula = newXml.GetElementsByTagName("aula");
                ModoAula = tmpNodoModoAula[0].InnerText.Equals("true");
                Debug.LogError("Read modo aula " + ModoAula);
                Debug.LogError("Read url_aula " + url_aula);
                //refBoxMessageManager.MtdCreateBoxMessageInfo(" Aula_confi->modoAula="+ModoAula+" Url="+url_aula, "ACEPTAR");
            }
            catch (Exception e)
            {
                Debug.Log("no existe el archivo alua_conf"); //efBoxMessageManager.MtdCreateBoxMessageInfo("No se pudo encontrar el archivo de configuracion esta es la direccion de la busqueda \n"+fileName, "ACEPTAR");
            }

        }


        /// <summary>
        ///  entrega la ubicacion correcta del archivo de aula dependeiendo de la plataforma
        /// </summary>
        private void CargarExternalXML()
        {

            fileName = "";
#if UNITY_IPHONE
		    fileName = Application.persistentDataPath + "/" + "aula_conf.xml"; 
		    openXML();
#elif UNITY_ANDROID
		   //var _dir = "/storage/emulated/0/";
		   //DirectoryInfo currentDirectory = new DirectoryInfo (_dir);
		   //ProcessDirectory(currentDirectory);
            try
            {
                var _dir = "/storage/emulated/0/";
                var _dir0 = "/storage/sdcard/";
                var _dir1 = "/storage/sdcard0/";
                var _dir2 = "/storage/sdcard1/";
                var _dirx = "/sdcard/";
                DirectoryInfo currentDirectory = new DirectoryInfo(_dir);
                DirectoryInfo currentDirectory_0 = new DirectoryInfo(_dir0);
                DirectoryInfo currentDirectory_1 = new DirectoryInfo(_dir1);
                DirectoryInfo currentDirectory_2 = new DirectoryInfo(_dir2);
                DirectoryInfo currentDirectory_x = new DirectoryInfo(_dirx);

                if (currentDirectory.Exists)
                {
                    ProcessDirectory(currentDirectory);
                }
                else if (currentDirectory_0.Exists)
                {

                    ProcessDirectory(currentDirectory_0);
                }
                else if (currentDirectory_1.Exists)
                {

                    ProcessDirectory(currentDirectory_1);
                }
                else if (currentDirectory_2.Exists)
                {

                    ProcessDirectory(currentDirectory_2);
                }
                else if (currentDirectory_x.Exists)
                {

                    ProcessDirectory(currentDirectory_x);
                }
            }catch (Exception error)
            {
                Debug.Log("no se encontro direccion viable");
            }
            
#elif UNITY_EDITOR
            fileName = Application.dataPath + "/" + "../../aula_conf.xml";
            openXML();
#elif UNITY_STANDALONE_OSX
		    fileName = Application.dataPath + "/" + "../../aula_conf.xml"; 
		    openXML();
#elif UNITY_STANDALONE_WIN
		    fileName = Application.dataPath + "/" + "../../../../aula_conf.xml";
		    openXML();
#endif
        }

        //--------------------------------------------------------------------loguin-------------------------------------------------------------------------------

        /// <summary>
        /// encripta caracteres 
        /// </summary>
        /// <param name="strToEncrypt"></param>
        /// <returns></returns>
        private string Md5Sum(string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
            byte[] bytes = ue.GetBytes(strToEncrypt);

            // encrypt bytes
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);

            // Convert the encrypted bytes back to a string (base 16)
            string hashString = "";

            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }

            return hashString.PadLeft(32, '0');
        }


        /// <summary>
        /// ordena los datos para la consulta de loguin
        /// </summary>
        private void aulaLoginRequest()
        {

            var _sesion_usuario = usuario;
            var _sesion_curso_input = curso;

            string _params;
            //string _params = "{\"user\":\"" + _sesion_usuario + "\",\"pass\":\"" + Md5Sum(_sesion_curso_input) + "\"}";
            

            //string _params = "{\"user\":\"" + UserInputElement.text + "\",\"pass\":\"" + Md5Sum(CourseInputElement.text) + "\"}";
#if UNITY_WEBGL
            _params = "{\"user\":\"" + _sesion_usuario + "\",\"pass\":\"" + _sesion_curso_input + "\"}";
#else
            _params = "{\"user\":\"" + _sesion_usuario + "\",\"pass\":\"" + Md5Sum(_sesion_curso_input) + "\"}";
#endif
            Debug.Log("usuario: " + _sesion_usuario);
            Debug.Log("pasword: " + _sesion_curso_input);

            byte[] bytesToEncode = Encoding.UTF8.GetBytes(_params);
            string encodedText = Convert.ToBase64String(bytesToEncode);

            string url_get_aula = url_aula + "/externals/login?data=" + encodedText;

            Debug.Log(url_get_aula);

            Debug.Log("El direccion enviada= " + _params);

            WWW wwwAula = new WWW(url_get_aula);
            StartCoroutine(WaitForRequestAula(wwwAula));


        }
        private void CamBioModoOfline()
        {
            PanelModoAula.ActivarPanel(false);
            PanelOffline.ActivarPanel(true);
        }


        public void cerrar()
        {
            Application.Quit();
        }

#endregion
    }
}
