﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NSInterfazAvanzada;

namespace NSEvaluacion
{
    /// <summary>
    /// Contiene las preguntas de evaluacion de cada una de las situaciones
    /// </summary>
	public class ControladorPreguntasSituacionales : MonoBehaviour
	{
        #region members
        /// <summary>
        /// Array que contiene las posibles preguntas para la situacion de la recta
        /// </summary>
        [SerializeField]
        private PreguntasSituacion[] preguntasSituacionRecta;
        /// <summary>
        /// Array que contiene las posibles preguntas para la situacion de la parabola
        /// </summary>
        [SerializeField]
        private PreguntasSituacion[] preguntasSituacionParabola;
        /// <summary>
        /// Array que contiene las posibles preguntas para la situacion de la circunferencia
        /// </summary>
        [SerializeField]
        private PreguntasSituacion[] preguntasSituacionCircunferencia;
        /// <summary>
        /// Array que contiene las posibles preguntas para la situacion de la hiperbola
        /// </summary>
        [SerializeField]
        private PreguntasSituacion[] preguntasSituacionHiperbola;
        /// <summary>
        /// Array que contiene las posibles preguntas para la situacion de la ellipse
        /// </summary>
        [SerializeField]
        private PreguntasSituacion[] preguntasSituacionEllipse;

        private PreguntasSituacion[] preguntasSituacionSeleccionada;
        #endregion

        #region public methods

        /// <summary>
        /// Conseguir las preguntas de evaluacion de la situacion.
        /// </summary>
        /// <returns>Objeto al azar que contiene imagen del enunciado, texto del enunciado, preguntas con respecto a ese enunciado para mostrar en la ventana de evaluación</returns>
        public PreguntasSituacion GetPreguntasSituacion()
        {
            return preguntasSituacionSeleccionada[Random.Range(0, preguntasSituacionSeleccionada.Length)];
        }

        public void SetPreguntasSituacion(SituacionSeleccionada argSituacionSeleccionada)
        {
            switch (argSituacionSeleccionada)
            {
                case SituacionSeleccionada.recta:
                    preguntasSituacionSeleccionada = preguntasSituacionRecta;
                    break;

                case SituacionSeleccionada.parabola:
                    preguntasSituacionSeleccionada = preguntasSituacionParabola;
                    break;

                case SituacionSeleccionada.circunferencia:

                    preguntasSituacionSeleccionada = preguntasSituacionCircunferencia;
                    break;

                case SituacionSeleccionada.elipse:
                    preguntasSituacionSeleccionada = preguntasSituacionEllipse;
                    break;

                case SituacionSeleccionada.hiperbola:
                    preguntasSituacionSeleccionada = preguntasSituacionHiperbola;
                    break;
            }
        }
        #endregion
    }
}